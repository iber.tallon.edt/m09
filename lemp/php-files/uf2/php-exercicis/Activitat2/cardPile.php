<?php
require_once 'card.php';

class CardPile {
    // Properties
    private array $pile;


    // Constructor 
    function __construct(){
        $this->pile = array();
    }

    // Getters and setters
    function getPile() :array {
        return $this->pile;
    }

    // Methods
    // Function to add a card to the pile
    public function addCard(Card $card){
        $this->pile[] = $card;
    }

    // Function to take a card from the pile
    public function removeCard(Card $card): Card {
        $key = array_search($card, $this->pile);
        if ($key !== false) {
            $removedCard = array_splice($this->pile, $key, 1)[0];
            return $removedCard;
        } else {
            return null;
        }
        
    }

}



?>