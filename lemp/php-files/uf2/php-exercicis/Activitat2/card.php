<?php
class Card {
    // Properties
    private int $value;
    private int $suit;
    private string $image;
 
 
    // Constructor 
    function __construct(int $value, int $suit, string $image){
        $this->value = $value;
        $this->suit = $suit;
        $this->image = $image;
    }


    // Getters and setters
    function getValue() : int{
        return $this->value;
    }

    function setValue(int $value) : void {
        $this->value = $value;
    }

    function getSuit() : int{
        return $this->suit;
    }

    function setSuit(int $suit) : void {
        $this->suit = $suit;
    }

    function getImage() : string{
        return $this->image;
    }

    function setImage(string $image) : void {
        $this->image = $image;
    }

    
    // toString
    public function __toString(): string{
        return "Card:\n" . 
        "Value: " . $this->getValue() . 
        "Suit: " . $this->getSuit() . 
        "Image: " . $this->getImage();   
    }

}

?>