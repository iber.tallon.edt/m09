<?php
include_once("card.php");
include_once("hand.php");
include_once("cardPile.php");
$hand = new Hand();
if ($_POST) {
    if (isset($_POST['giveCards'])) {
        $cardPile = generateCards();
        $hand = getCards($cardPile);
        showCards($hand);
        $text = getPoints($hand);
        showPoints($text);
        $total = getTotal($hand);
        showTotal($total);
        evaluate($hand);
    }
}


function generateCards(): CardPile
{
  $cardPile = new CardPile();
  for ($i=1; $i <= 13; $i++) {
    for ($j=1; $j <= 4 ; $j++) { 
        $card = new Card($i, $j, "cards/card" . ($i + ($j - 1) * 13 ) . ".gif");
        $cardPile->addCard($card);
    } 

  }
  return $cardPile;
}


function getCards(CardPile $cardPile): Hand
{
    $hand = new Hand();
    for ($i = 0; $i < 5; $i++) {
      $index = array_rand($cardPile->getPile());
      $carta_random = $cardPile->getPile()[$index];
      $hand->addCard($carta_random);
      $cardPile->removeCard($carta_random);
    }
    return $hand;
}


function showCards(Hand &$hand){
  echo "<h3>Poker hand</h3>";
    foreach ($hand->getCards() as $card) {
        echo '<img src="' . $card->getImage() . '" alt="Card" style="max-width: 150px; margin: 5px;">';
    }
}


function getPoints($hand): string {
    $points = [];
    foreach ($hand->getCards() as $card) {
        $number = $card->getValue();
        
        if ($number == 1) { 
            $number = 14;

        }
        
        $points[] = $number;
    }
    return implode(' ', $points);
}


function showPoints($text){
    echo "<br></br>";
    echo "<h2>Result</h2>";
    echo "<p>CARD POINTS: $text</p>";
}


function getTotal(&$hand): int {
    $suma = 0;
    $points_getPoints = getPoints($hand); 
    $points = explode(' ', $points_getPoints); 
    foreach ($points as $point) {
        $suma += $point;
    }
    return $suma;
}


function showTotal($total){
    echo "<p>TOTAL POINTS: $total</p>";
}


function evaluate(&$hand) {
    if (isRoyalFlush($hand)) {
        echo "Result: ROYAL FLUSH";
    } elseif (isStraightFlush($hand)) {
        echo "Result: STRAIGHT FLUSH";
    } elseif (isRepoker($hand)) {
        echo "Result: FOUR OUR OF A KIND";
    } elseif (isPoker($hand)) {
        echo "Result: POKER";
    } elseif (isFullHouse($hand)) {
        echo "Result: FULL HOUSE";
    } elseif (isFlush($hand)) {
        echo "Result: FLUSH";
    } elseif (isStraight($hand)) {
        echo "Result: STRAIGHT";
    } elseif (isThreeOfAKind($hand)) {
        echo "Result: TRHEE OF A KIND";
    } elseif (isTwoPairs($hand)) {
        echo "Result: TWO PAIRS";
    } elseif (isOnePair($hand)) {
        echo "Result: ONE PAIR";
    } else {
        echo "Result: HIGH CARD";
    }
}


function isRoyalFlush($hand): bool {
    $cards = $hand->getCards();
    $mateixpal = true;
    $primera = $cards[0]->getSuit();
    foreach ($cards as $card) {
        if ($card->getSuit() != $primera) {
            $mateixpal = false;
            break;
        }
    }
    $combinacio = [1, 10, 11, 12, 13];

    $values = array_map(function ($card) {
        return $card->getValue();
    }, $cards);
    sort($values);
    return $mateixpal && $values == $combinacio;
}


function isStraightFlush($hand): bool {
    $cards = $hand->getCards();
    $sameSuit = true;
    $firstSuit = $cards[0]->getSuit();
    foreach ($cards as $card) {
        if ($card->getSuit() != $firstSuit) {
            $sameSuit = false;
            break;
        }
    }
    if (!$sameSuit) {
        return false;
    }
    $values = [];
    foreach ($cards as $card) {
        $values[] = $card->getValue();
    }
    $validSequences = [
        [1, 2, 3, 4, 5],
        [2, 3, 4, 5, 6],
        [3, 4, 5, 6, 7],
        [4, 5, 6, 7, 8],
        [5, 6, 7, 8, 9],
        [6, 7, 8, 9, 10],
        [7, 8, 9, 10, 11],
        [8, 9, 10, 11, 12],
        [9, 10, 11, 12, 13]
    ];
    foreach ($validSequences as $sequence) {
        if ($values === $sequence) {
            return true;
        }
    }
    return false;
}


function isRepoker($hand): bool{
    $points_getPoints = getPoints($hand); 
    $values = explode(' ', $points_getPoints); 
    $uniqueValues = array_unique($values);
    return count($uniqueValues) === 1;
}


function isPoker($hand): bool {
    $points_getPoints = getPoints($hand); 
    $values = explode(' ', $points_getPoints); 
    sort($values);
    for ($i = 0; $i <= 1; $i++) { 
        if ($values[$i] == $values[$i+1] && $values[$i] == $values[$i+2] && $values[$i] == $values[$i+3]){
            return true;
        }
    }

    return false;
}

function isFullHouse($hand): bool
{
    $points_getPoints = getPoints($hand);
    $values = explode(' ', $points_getPoints); 

    sort($values);

    for ($i = 0; $i <= count($values) - 5; $i++) {
        if (($values[$i] == $values[$i+1] && $values[$i+1] == $values[$i+2] && $values[$i+3] == $values[$i+4])
            || ($values[$i] == $values[$i+1] && $values[$i+2] == $values[$i+3] && $values[$i+3] == $values[$i+4])) {
            return true;
        }
    }

    return false;
}


function isFlush($hand): bool {
    $cards = $hand->getCards();
    
    if (empty($cards)) {
        return false;
    }

    $primera = $cards[0]->getSuit(); 
    foreach ($cards as $card) {
        if ($card->getSuit() !== $primera) {
            return false;
        }
    }

    return true;
}


function isStraight($hand): bool {
    $cards = $hand->getCards();

    $values = [];
    foreach ($cards as $card) {
        $value = $card->getValue();
        if ($value == 1) {
            $values[] = 1;  
            $values[] = 14; 
        } else {
            $values[] = $value;
        }
    }

    $validSequences = [
        [14, 2, 3, 4, 5],
        [2, 3, 4, 5, 6],
        [3, 4, 5, 6, 7],
        [4, 5, 6, 7, 8],
        [5, 6, 7, 8, 9],
        [6, 7, 8, 9, 10],
        [7, 8, 9, 10, 11],
        [8, 9, 10, 11, 12],
        [9, 10, 11, 12, 13],
        [14, 10, 11, 12, 13] 
    ];

    foreach ($validSequences as $sequence) {
        sort($sequence);
        if ($values === $sequence) {
            return true;
        }
    }

    return false;
}



function isThreeOfAKind($hand): bool{
    $points_getPoints = getPoints($hand); 
    $points = explode(' ', $points_getPoints); 

    sort($points);

    for ($i = 0; $i <= 2; $i++) {
        if ($points[$i] == $points[$i + 1] && $points[$i + 1] == $points[$i + 2]) {

            return true;
        }
    }

    return false;
}


function isTwoPairs($hand): bool {
    $points_getPoints = getPoints($hand); 
    $points = explode(' ', $points_getPoints); 

    sort($points); 

    $pairsCount = 0;
    for ($i = 0; $i < 4; $i++) {
        if ($points[$i] == $points[$i+1]) {
            $pairsCount++;
        }
    }

    return $pairsCount == 2;
}



function isOnePair($hand): bool{
    $points_getPoints = getPoints($hand); 
    $points = explode(' ', $points_getPoints); 

    sort($points); 

    for ($i = 0; $i < 4; $i++) {
        if ($points[$i] == $points[$i+1]) {
            return true;
        } 
    }

    return false;
}

?>