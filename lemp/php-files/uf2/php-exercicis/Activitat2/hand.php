<?php
require_once 'card.php';
class Hand {
    // Properties
    private array $cards;


    // Constructor 
    function __construct(){
        $this->cards = array();
    }


    // Getters and setters
    function getCards() : array {
        return $this->cards;
    }

    function setCards(array $cards) : void{
        $this->cards = $cards;
    }


    // Methods
    public function addCard(Card $card){
        $this->cards[] = $card;
    }

    // toString
    public function __toString(){
        return "Hand:\n" . 
        "Card: " . $this-> getCards();
    }

}

?>