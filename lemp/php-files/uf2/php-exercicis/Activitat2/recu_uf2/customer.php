<?php
require_once('address.php');

class Customer
{
    // Properties
    private int $customer_id;
    private string $first_name;
    private string $last_name;
    private string $email;
    private array $addresses;

    // Constructor
    function __construct(int $customer_id, string $first_name, string $last_name, string $email)
    {
        $this->customer_id = $customer_id;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->email = $email;
        $this->addresses = array();
    }

    // Getters
    function get_customer_id(): int {
        return $this->customer_id;
    }
    function get_first_name(): string {
        return $this->first_name;
    } 
    function get_last_name(): string {
        return $this->last_name;
    }
    function get_email(): string {
        return $this->email;
    }
    function get_addresses(): array {
        return $this->addresses;
    }

    // Setters
    function set_customer_id(int $customer_id): void {
        $this->customer_id = $customer_id;
    }
    function set_first_name(string $first_name): void {
        $this->first_name = $first_name;
    }
    function set_last_name(string $last_name): void {
        $this->last_name = $last_name;
    }
    function set_email(string $email): void {
        $this->email = $email;
    }

    // Method
    function addresses(Address $address) {
        $this->addresses[] = $address;
    }

    // toString
    public function __toString(): string
    {
        return "Customer: " . 
        "Customer id: " . $this->get_customer_id() . "\n" . 
        "First name: " . $this->get_first_name() . "\n" . 
        "Last name: " . $this->get_last_name() . "\n" . 
        "Email: " . $this->get_email() . "\n" . 
        "Addresses: " . $this->get_addresses() . "\n";        
    }

}
?>