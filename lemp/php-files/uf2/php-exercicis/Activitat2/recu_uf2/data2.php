<?php   

// Importar classes
include_once("customer.php");

$servername = "172.29.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

// Crear la connexió
$conn = new mysqli($servername, $username, $password, $dbname);

// Mirar estat de la connexió
if ($conn->connect_error) {
    die("Connection failed: " . $conn -> connect_error);
} else {
    echo("Successfully connected" . "<br/>");
}

echo "<h1>Name Search</h1>";
echo "<br>";

echo "<form>";
    echo "<input type='text' name='customer'>";
    echo "<button class='boton' type='submit' name='execute'>SEARCH NAME</button>";
echo "</form>";

// funció que busca els customers
function execute($conn, $customer) {
    $sql = "SELECT * FROM `customer` c
    WHERE LOWER(c.first_name) LIKE CONCAT('%', ?, '%')
    OR LOWER(c.last_name) LIKE CONCAT('%', ?, '%')
    ORDER BY c.customer_id;";

    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ss", $customer, $customer);
    $stmt->execute();
    $result = $stmt->get_result();

    echo "<h2>Customers 2</h2>";

    echo "Number of records: " . $result->num_rows . "<br>";

    echo "<table class='table'>";
        echo "<tr>";
            echo "<td>customer_id</td>";
            echo "<td>first_name</td>";
            echo "<td>last_name</td>";
            echo "<td>email</td>";
        echo "</tr>";

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $customer = new Customer($row["customer_id"], $row["first_name"], $row["last_name"], $row["email"]);
            
                echo "<tr>";
                    echo "<td>" . $customer->get_customer_id() . "</td>";
                    echo "<td>" . $customer->get_first_name() . "</td>";
                    echo "<td>" . $customer->get_last_name() . "</td>";
                    echo "<td>" . $customer->get_email() . "</td>";
                echo "</tr>";
            }
        }
    echo "</table>";  
    $stmt->close();
}

if (isset($_GET['execute'])) {
    $customer = $_GET['customer'];
    execute($conn, $customer);
}


$conn->close();

?>