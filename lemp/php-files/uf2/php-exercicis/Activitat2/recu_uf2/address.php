<?php

require_once("country.php");

class Address

// Propieties
{
    private int $address_id;
    private string $street_number;
    private string $street_name;
    private string $city;
    private Country $country;


// Constructor
function __construct(int $address_id, string $street_number, string $street_name, string $city, Country $country){
    $this ->address_id = $address_id;
    $this ->street_number = $street_number;
    $this ->street_name = $street_name;
    $this ->city = $city;
    $this ->country = $country;
}


// Getters
function get_address_id(): int {
    return $this -> address_id;
}
function get_street_number(): string {
    return $this ->street_number;
}
function get_street_name(): string {
    return $this ->street_name;
}
function get_city(): string{
    return $this -> city;
}
function get_country(): Country {
    return $this -> country;
}


// Setters
function set_address_id(int $address_id): void {
    $this -> address_id = $address_id;
}
function set_street_number(string $street_number): void {
    $this ->street_number = $street_number;
}
function set_street_name(string $street_name): void {
    $this ->street_name = $street_name;
}
function set_city(string $city): void {
    $this ->city = $city;
}
function set_country(Country $country): void {
    $this -> country = $country;
}


// toString
public function __toString(): string {
    return "Address: " . "\n " .
    "Address id: " . $this -> get_address_id() . "\n " .
    "Street number: " . $this -> get_street_number() . "\n " .
    "Street name: " . $this -> get_street_name() . "\n " .
    "City" . $this -> get_city() . "\n " .
    "Country: " . $this -> get_country() . "\n ";
}
}

?>