<?php
require_once 'customer.php';
require_once 'country.php';
require_once 'address.php';


$servername = "172.29.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

// Crear la connexió
$conn = new mysqli($servername, $username, $password, $dbname);

// Mirar estat de la connexió
if ($conn->connect_error) {
    die("Connection failed: " . $conn -> connect_error);
} else {
    echo("Successfully connected" . "<br/>");
}

echo "<h1>ID Search</h1>";
echo "<br>";

echo "<form>";
    echo "<input type='text' name='customer'>";
    echo "<button class='boton' type='submit' name='execute'>ENTER ID</button>";
echo "</form>";

// funció que busca els customers
function execute($conn, $customer) {

    // Primera consulta (taula1)
    $sql1 = "SELECT * FROM `customer` c
    WHERE customer_id = ?
    ORDER BY c.customer_id;";

    $stmt = $conn->prepare($sql1);
    $stmt->bind_param("i", $customer);
    $stmt->execute();
    $result = $stmt->get_result();

    echo "<h2>Customers 3</h2>";

    echo "Number of records: " . $result->num_rows . "<br>";

    echo "<table class='table'>";
        echo "<tr>";
            echo "<td>customer_id</td>";
            echo "<td>first_name</td>";
            echo "<td>last_name</td>";
            echo "<td>email</td>";
        echo "</tr>";

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $customer = new Customer($row["customer_id"], $row["first_name"], $row["last_name"], $row["email"]);
            
                echo "<tr>";
                    echo "<td>" . $customer->get_customer_id() . "</td>";
                    echo "<td>" . $customer->get_first_name() . "</td>";
                    echo "<td>" . $customer->get_last_name() . "</td>";
                    echo "<td>" . $customer->get_email() . "</td>";
                echo "</tr>";
            }
        }
    echo "</table>";  
    echo "<br>";
    // Segona consulta (taula1)
    $sql2 = "SELECT * FROM `address` a
    INNER JOIN customer_address ca ON ca.address_id = a.address_id
    INNER JOIN country c ON c.country_id = a.country_id
    INNER JOIN address_status ast ON ast.status_id = ca.status_id
    WHERE ca.customer_id = ?
    ORDER BY a.address_id;";

    $stmt2 = $conn->prepare($sql2);
    $stmt2->bind_param("i", $customer);
    $stmt2->execute();
    $result2 = $stmt2->get_result();

    echo "<table class='table'>";
        echo "<tr>";
            echo "<td>street name</td>";
            echo "<td>street number</td>";
            echo "<td>city</td>";
            echo "<td>country</td>";
        echo "</tr>";

        if ($result2->num_rows > 0) {
            while ($row2 = $result2->fetch_assoc()) {
                  $country = new Country($row2["country_id"], $row2["country_name"]);
                $address = new Address($row2["street_number"], $row2["street_name"], $row2["city"], "",$country );
              
                echo "<tr>";
                    echo "<td>" . $address->get_street_number() . "</td>";
                    echo "<td>" . $address->get_street_name() . "</td>";
                    echo "<td>" . $address->get_city() . "</td>";
                    echo "<td>" . $country->get_country_name() . "</td>";
                echo "</tr>";
            }
        }






    $stmt->close();
}

if (isset($_GET['execute'])) {
    $customer = $_GET['customer'];
    execute($conn, $customer);
}


$conn->close();

?>