<?php
    class Pokemon {
        // Propiedades o atributos
        private int $code;
        private string $name;
        private string $type1;
        private string $type2;
        private int $healthPoints;
        private int $attack;
        private int $defense;
        private int $specialAttack;
        private int $specialDefense;
        private int $speed;
        private int $generation;
        private bool $legendary;
        private string $image;
        private int $total;
    
        // Constructor
        function __construct(int $code, 
                            string $name, 
                            string $type1, 
                            string $type2, 
                            int $healthPoints,
                            int $attack,
                            int $defense, 
                            int $specialAttack,
                            int $specialDefense, 
                            int $speed,
                            int $generation,
                            bool $legendary,
                            string $image){
            $this->code = $code;
            $this->name = $name;
            $this->type1 = $type1;
            $this->type2 = $type2;
            $this->healthPoints = $healthPoints;
            $this->attack = $attack;
            $this->defense = $defense;
            $this->specialAttack = $specialAttack;
            $this->specialDefense = $specialDefense;
            $this->speed = $speed;
            $this->generation = $generation;
            $this->legendary = $legendary;
            $this->image = $image;  
            $this->total = $this->total();            
        }
    
        // Métodos getters y setters
        function get_code(): int {
            return $this->code;
        }
    
        function set_code(int $code): void {
            $this->code = $code;
        }
    
        function get_name(): string {
            return $this->name;
        }
    
        function set_name(string $name): void {
            $this->name = $name;
        }
    
        function get_type1(): string {
            return $this->type1;
        }
    
        function set_type1(string $type1): void {
            $this->type1 = $type1;
        }
    
        function get_type2(): string {
            return $this->type2;
        }
    
        function set_type2(string $type2): void {
            $this->type2 = $type2;
        }
    
        function get_healthPoints(): int {
            return $this->healthPoints;
        }
    
        function set_healthPoints(int $healthPoints): void {
            $this->healthPoints = $healthPoints;
        }
    
        function get_attack(): int {
            return $this->attack;
        }
    
        function set_attack(int $attack): void {
            $this->attack = $attack;
        }
    
        function get_defense(): int {
            return $this->defense;
        }
    
        function set_defense(int $defense): void {
            $this->defense = $defense;
        }
    
        function get_specialAttack(): int {
            return $this->specialAttack;
        }
    
        function set_specialAttack(int $specialAttack): void {
            $this->specialAttack = $specialAttack;
        }
    
        function get_specialDefense(): int {
            return $this->specialDefense;
        }
    
        function set_specialDefense(int $specialDefense): void {
            $this->specialDefense = $specialDefense;
        }
    
        function get_speed(): int {
            return $this->speed;
        }
    
        function set_speed(int $speed): void {
            $this->speed = $speed;
        }
    
        function get_generation(): int {
            return $this->generation;
        }
    
        function set_generation(int $generation): void {
            $this->generation = $generation;
        }
    
        function get_legendary(): bool {
            return $this->legendary;
        }
    
        function set_legendary(bool $legendary): void {
            $this->legendary = $legendary;
        }
    
        function get_image(): string {
            return $this->image;
        }
    
        function set_image(string $image): void {
            $this->image = $image;
        }
    
        function get_total(): int {
            return $this->total;
        }
    
        function set_total(int $total): void {
            $this->total = $total;
        }
    
        
        // toString
        function __toString(): string {
            return "Pokemon:\n" .
            "Code: " . $this->get_code() .
            "Name: " . $this->get_name() .
            "Type 1: " . $this->get_type1() .
            "Type 2: " . $this->get_type2() .
            "Health Points: " . $this->get_healthPoints() .
            "Attack: " . $this->get_attack() .
            "Defense: " . $this->get_defense() .
            "Special Attack: " . $this->get_specialAttack() .
            "Special Defense: " . $this->get_specialDefense() .
            "Speed: " . $this->get_speed() .
            "Generation: " . $this->get_generation() .
            "Legendary: " . $this->get_legendary() .
            "Image: " . $this->get_image() .
            "Total: " . $this->get_total();
        }
    
        function total(): int {
            return $this->healthPoints + 
            $this->attack + 
            $this->defense + 
            $this->specialAttack + 
            $this->specialDefense + 
            $this->speed;
        }
    }
    
?>