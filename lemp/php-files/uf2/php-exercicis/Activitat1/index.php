<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activitat 1</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class='container'>
        <?php
                include_once 'pokemon.php';
                include_once 'pokemons.php';

                $pokemons = new Pokemons();

                $url = "https://joanseculi.com/json/pokemons.json";

                $json_data = file_get_contents($url);
                $data = json_decode($json_data, true);

                foreach ($data['pokemons'] as $pokemon_data) {
                    $pokemon = new Pokemon(
                        $pokemon_data['Code'],
                        $pokemon_data['Name'],
                        $pokemon_data['Type1'],
                        $pokemon_data['Type2'],
                        $pokemon_data['HealthPoints'],
                        $pokemon_data['Attack'],
                        $pokemon_data['Defense'],
                        $pokemon_data['SpecialAttack'],
                        $pokemon_data['SpecialDefense'],
                        $pokemon_data['Speed'],
                        $pokemon_data['Generation'],
                        $pokemon_data['Legendary'],
                        $pokemon_data['Image']
                    );
                    $pokemons->add_pokemon($pokemon);
                }

                echo "<table>"; 
                    echo "<form method='POST' action='" . htmlspecialchars($_SERVER['PHP_SELF']) . "'>";
                        echo "<tr>";
                            echo "<td>Generation: </td>";
                            echo "<td>";
                                        echo "<select name='generacio' class='form-control'>";
                                        echo "<option value='All'>All</option>";
                                        for ($i = 1; $i <= 6; $i++) {
                                            $selected = isset($_POST['generacio']) && $_POST['generacio'] == $i ? 'selected' : '';
                                            echo "<option value='$i' $selected>$i</option>";
                                        }
                                        echo "</select>";
                                        echo "<button class='btn btn-primary' type='submit' name='execute'>Search</button>";
                            echo "</td>";
                        echo "</tr>";
                        echo "<tr>";
                            echo "<td>Search pokemons: </td>";
                            echo "<td>";
                                        echo "<input type='text' name='pokemon_name' class='form-control' size='60' 
                                        value='" . (isset($_POST['pokemon_name']) ? $_POST['pokemon_name'] : '') . "'>"; 
                                    echo "<button class='btn btn-primary' type='submit' name='execute_name'>Search</button>";
                            echo "</td>";
                        echo "</tr>";

                        echo "<tr>";
                            echo "<td>Search Type: </td>";
                            echo "<td>";
                                        echo "<input type='text' name='pokemon_type' class='form-control' size='60' 
                                        value='" . (isset($_POST['pokemon_type']) ? $_POST['pokemon_type'] : '') . "'>"; 

                                        echo "<button class='btn btn-primary' type='submit' name='execute_type'>Search</button>";
                            echo "</td>";
                        echo "</tr>";
                    echo "</form>";

                echo "<br><br/>";
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    if (isset($_POST['execute'])) {
                        generacio($pokemons);
                    }
                    if (isset($_POST['execute_name'])) {
                        nom($pokemons);
                    }
                    if (isset($_POST['execute_type'])) {
                        tipus($pokemons);
                    }
                }


                function generacio($pokemons) {
                    $seleccionem_generacio = $_POST["generacio"];

                    if ($seleccionem_generacio == "All") {
                        $filtrar_pokemons = $pokemons->get_pokemons();
                    } else {
                        $filtrar_pokemons = $pokemons->get_generation($seleccionem_generacio);
                    }

                    pokemons_taula($filtrar_pokemons);
                }

                function nom($pokemons) {
                    $nom = $_POST["pokemon_name"];
                    $filtrar_pokemons = $pokemons->get_pokemon_name($nom);
                    
                    if (!empty($filtrar_pokemons)) {
                        pokemons_taula($filtrar_pokemons);
                    } else {
                        echo "No s'ha trobat cap nom";
                    }
                }

                function tipus($pokemons) {
                    $tipus = $_POST["pokemon_type"];
                    $filtrar_pokemons = $pokemons->get_pokemon_type($tipus);
                    
                    if (!empty($filtrar_pokemons)) {
                        pokemons_taula($filtrar_pokemons);
                    } else {
                        echo "No s'ha trobat cap tipus";
                    }
                }



                function pokemons_taula($filtrar_pokemons) {
                    echo "<table class='table table-striped table-primary'>";
                    echo "<br><br/>";
                    echo "<thead class='table-success'>";
                    echo "<tr>";
                    echo "<th>Image</th>";
                    echo "<th>Code</th>";
                    echo "<th>Name</th>";
                    echo "<th>Type 1</th>";
                    echo "<th>Type 2</th>";
                    echo "<th>HP</th>";
                    echo "<th>ATK</th>";
                    echo "<th>DEF</th>";
                    echo "<th>SP_ATK</th>";
                    echo "<th>SP_DEF</th>";
                    echo "<th>SPD</th>";
                    echo "<th>GEN</th>";
                    echo "<th>LEG</th>";
                    echo "<th>TOTAL</th>";
                    echo "</tr>";
                    echo "</thead>";
                    echo "<tbody'>";

                    foreach ($filtrar_pokemons as $pokemon) {
                        echo "<tr>";
                        echo "<td><img src='{$pokemon->get_image()}' alt='{$pokemon->get_name()}' width='200'></td>";
                        echo "<td>{$pokemon->get_code()}</td>";
                        echo "<td>{$pokemon->get_name()}</td>";
                        echo "<td>{$pokemon->get_type1()}</td>";
                        echo "<td>{$pokemon->get_type2()}</td>";
                        echo "<td>{$pokemon->get_healthPoints()}</td>";
                        echo "<td>{$pokemon->get_attack()}</td>";
                        echo "<td>{$pokemon->get_defense()}</td>";
                        echo "<td>{$pokemon->get_specialAttack()}</td>";
                        echo "<td>{$pokemon->get_specialDefense()}</td>";
                        echo "<td>{$pokemon->get_speed()}</td>";
                        echo "<td>{$pokemon->get_generation()}</td>";
                        echo "<td>" .($pokemon->get_legendary() ? 'Yes' : 'No') . "</td>";
                        echo "<td>{$pokemon->get_total()}</td>";
                        echo "</tr>";
                    }
                    echo "</tbody>";
                    echo "</table>";
                }

        ?>
    </div> 
</body>
</html>
