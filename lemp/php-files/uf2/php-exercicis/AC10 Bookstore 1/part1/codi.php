<?php
    $servername = "172.22.0.2";     
    $username = "root";
    $password = "1234";
    $dbname = "bookstore";
    
    $conn = new mysqli($servername, $username, $password, $dbname);


    if ($conn->connect_error) {
        die("La connexió a fallat". $conn->connect_error);
    }

    $sql = "SELECT book_id, title, isbn13, publication_date FROM book";

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        echo "<table>";
        echo "<thead>";
            echo "<tr>";
                echo "<th class='id'>ID</th>";
                echo "<th class='title'>title</th>";
                echo "<th class='isbn'>ISBN13</th>";
                echo "<th class='publi'>PUBL DATE</th>";
            echo "</tr>";
        echo "</thead>"; 
        echo "<tbody>";
        while ($row = $result->fetch_assoc()) {
       
            echo "<tr>";
                echo "<td class='id'>" . $row["book_id"] . "</td>";
                echo "<td class='title'>" . $row["title"] . "</td>";
                echo "<td class='isbn'>" . $row["isbn13"] . "</td>";
                echo "<td class='publi'>" . $row["publication_date"] . "<br />" . "</td>";
            echo "</tr>";

            }        
        echo "</tbody>";
        echo "</table>";
        }            
        $conn->close();
?>