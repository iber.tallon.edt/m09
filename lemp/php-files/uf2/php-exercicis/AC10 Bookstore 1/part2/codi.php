<?php
    $servername = "172.22.0.2";     
    $username = "root";
    $password = "1234";
    $dbname = "bookstore";

    function execute($id) {
        $sql = "SELECT book_id, title, isbn13, publication_date FROM book WHERE book_id LIKE 
        '%$id%' ORDER BY book_id";

        $result = $GLOBALS['conn']->query($sql);

        if ($result->num_rows > 0) {

            echo "<table>";
                    echo "<tr>";
                    echo "<td>ID</td>";
                    echo "<td>TITLE</td>";
                    echo "<td>ISBN13</td>";
                    echo "<td>PUBL. DATE</td>";
                    echo "</tr>";
                while ($row = $result->fetch_assoc()) {
                    echo "<tr>";
                        echo "<td>" . $row["book_id"] ."</td>";
                        echo "<td>". $row["title"] ."</td>";
                        echo "<td>". $row["isbn13"] ."</td>";
                        echo "<td>". $row["publication_date"] ."</td>";
                    echo "</tr>";
                } 
        }
        else {
            echo "0 resultats";
        }
            echo "</table>";
    }


    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
        die("Connexió fallida: " . $conn->connect_error);
    }

    echo "<form method='POST' action='" . htmlspecialchars($_SERVER['PHP_SELF']) . "'>";
    $id="";
    if (isset($_POST['execute'])) {
        $id = $_POST['id'];
    }

    echo "<input type='text' value='{$id}' placeholder='Enter ID' name='id' />";

    echo "<input type='submit' value='Select' name='execute' id='buton' class='buton' />";

    echo "</form>";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (isset($_POST['execute'])) {
            execute($_POST['id']);
        }
    }
    $conn->close();
?>