<?php
require_once 'language.php';
require_once 'publisher.php';
require_once 'author.php';
require_once 'book.php';
class Library
{
    // Properties
    private string $name;
    private array $books;


    // Constructor
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->books = [];
    }

    // Getters
    public function get_name(): string
    {
        return $this->name;
    }

    public function get_books(): array
    {
        return $this->books;
    }

    // Setters
    public function set_name(string $name): void
    {
        $this->name = $name;
    }

    public function set_books(array $books): void
    {
        $this->books = $books;
    }

    // Methods

    public function add_book(Book $book): void
    {
        $this->books[] = $book;
    }

    public function get_book_by_name(string $title): ?Book
    {
        foreach ($this->books as $book) {
            //Find position of first occurrence of a case-insensitive string

            if (stripos($book->get_title(), $title) === 0) {
                return $book;
            }
        }
        return null; // Return null if not found
    }

    public function get_books_by_name(string $title): array{
        $books = [];
        foreach ($this->books as $book) {
            //Find position of first occurrence of a case-insensitive string

            if (stripos($book->get_title(), $title) != false) {
                $books[] = $book;
            }
        }
        return $books;
    }


    // toString
    public function __toString(): string
    {
        $booksString = "";
        foreach ($this->books as $book) {
            $booksString .= $book . "\n\n"; // use __toString() of class Book 
        }

        return "Library: " . $this->name . "\n\n" .
            "Books:\n" . $booksString;
    }
}