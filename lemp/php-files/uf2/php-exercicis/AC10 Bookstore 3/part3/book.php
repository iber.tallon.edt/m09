<?php
require_once 'language.php'; 
require_once 'publisher.php'; 
require_once 'author.php'; 
class Book
{
    // Properties
    private int $book_id;
    private string $title;
    private string $isbn13;
    private Language $language; 
    private int $num_pages;
    private string $publication_date;
    private Publisher $publisher;
    private array $authors;  //This is an array of Author object class


    // Constructor 
   /* public function __construct(int $book_id = 0, string $title = "", string $isbn13 = "", Language $language = null, int $num_pages = 0, string $publication_date = "", Publisher $publisher = null)
    {
        $this->book_id = $book_id;
        $this->title = $title;
        $this->isbn13 = $isbn13;
        $this->language = $language;
        $this->num_pages = $num_pages;
        $this->publication_date = $publication_date;
        $this->publisher = $publisher;
        $this->authors = [];
    }
*/
    public function __construct()
    {
        $this->authors = [];
    }

    // Destructor
    /*
        function __destruct()
        {
            echo "The book is {$this}.";
        }
        */

    // Getters and setters
    // Getters
    public function get_book_id(): int
    {
        return $this->book_id;
    }

    public function get_title(): string
    {
        return $this->title;
    }

    public function get_isbn13(): string
    {
        return $this->isbn13;
    }

    public function get_language(): Language
    {
        return $this->language;
    }

    public function get_num_pages(): int
    {
        return $this->num_pages;
    }

    public function get_publication_date(): string
    {
        return $this->publication_date;
    }

    public function get_publisher(): Publisher
    {
        return $this->publisher;
    }

    public function get_authors(): array
    {
        return $this->authors;
    }

    // Setters
    public function set_book_id(int $book_id): void
    {
        $this->book_id = $book_id;
    }
    public function set_title(string $title): void
    {
        $this->title = $title;
    }

    public function set_isbn13(string $isbn13): void
    {
        $this->isbn13 = $isbn13;
    }

    public function set_language(Language $language): void
    {
        $this->language = $language;
    }

    public function set_num_pages(int $num_pages): void
    {
        $this->num_pages = $num_pages;
    }

    public function set_publication_date(string $publication_date): void
    {
        $this->publication_date = $publication_date;
    }

    public function set_publisher(Publisher $publisher): void
    {
        $this->publisher = $publisher;
    }

    public function set_authors(array $authors): void
    {
        $this->authors = $authors;
    }

    // Methods

    public function add_author(Author $author): void {
        $this->authors[] =  $author;
    }

    public function remove_author(Author $author): void {
        $this->authors = array_diff($this->authors, [$author]);
    }

    //toString
    public function __toString(): string
    {
        $authorsString = implode(", ", array_map(function ($author) {
            return $author->get_author_name(); 
        }, $this->authors));

        return "Book ID: " . $this->book_id . "\n" .
               "Title: " . $this->title . "\n" .
               "ISBN-13: " . $this->isbn13 . "\n" .
               "Language: " . $this->language->get_language_name() . "\n" . 
               "Number of Pages: " . $this->num_pages . "\n" .
               "Publication Date: " . $this->publication_date . "\n" .
               "Publisher: " . $this->publisher->get_publisher_name() . "\n" . 
               "Authors: " . $authorsString;
    }
}
?>