<?php

class Language
{
    // Properties
    private int $language_id;
    private string $language_code;
    private string $language_name;

    // Constructor 
    function __construct(int $language_id, string $language_code, string $language_name)
    {
        $this->language_id = $language_id;
        $this->language_code = $language_code;
        $this->language_name = $language_name;

    }

    // Getters and setters
    function set_language_id($language_id): void
    {
        $this->language_id = $language_id;
    }

    function get_language_id(): int
    {
        return $this->language_id;
    }

    function set_language_code($language_code): void
    {
        $this->language_code = $language_code;
    }

    function get_language_code(): string
    {
        return $this->language_code;
    }

    function set_language_name($language_name): void
    {
        $this->language_name = $language_name;
    }

    function get_language_name(): string
    {
        return $this->language_name;
    }

    // Methods

    //toString
    public function __toString(): string
    {
        return "Language[language_id= " . $this->get_language_id() . ", language_code= " . $this->get_language_code() . ", language_name=" . $this->get_language_name() . "]";
    }
}