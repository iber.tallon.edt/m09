<?php

class Author
{
    // Properties
    private int $author_id;
    private string $author_name;

    // Constructor 
    function __construct(int $author_id, string $author_name)
    {
        $this->author_id = $author_id;
        $this->author_name = $author_name;
    }

// Getters and setters
function set_author_id($author_id): void
{
    $this->author_id = $author_id;
}

function get_author_id(): int
{
    return $this->author_id;
}

function set_author_name($author_name): void
{
    $this->author_name = $author_name;
}

function get_author_name(): string
{
    return $this->author_name;
}


// Methods

//toString
public function __toString(): string
{
    return "Author[author_id= " . $this->get_author_id() . ", author_name= " . $this->get_author_name() . "]";
}
}

?>
