<?php
require_once 'language.php'; 
require_once 'publisher.php'; 
require_once 'author.php';
require_once 'book.php';
require_once 'library.php';

$servername = "172.22.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";


// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

$library = new Library("Library 1");

// Check connection
if ($conn->connect_error) {
    die("Connexió fallida: ". $conn->connect_error);
} else {
    $library = loadData($library);
}

function loadData(Library $library) : Library {
    $sql = "SELECT * FROM book b 
    INNER JOIN book_language bl ON b.language_id = bl.language_id 
    INNER JOIN publisher p ON b.publisher_id = p.publisher_id 
    INNER JOIN book_author ba ON b.book_id = ba.book_id 
    INNER JOIN author a ON a.author_id = ba.author_id 
    ORDER BY b.book_id;"; 
    
    $stmt = $GLOBALS['conn']->prepare($sql);
    $stmt->execute();
    $result = $stmt->get_result(); // get the mysqli result
    if ($result->num_rows > 0) {
        // output data of each row
        $last_book_id = 0;
        $last_book = new Book();

        while ($row = $result->fetch_assoc()) {
            //var_dump($row);
           // $publisher = new Publisher(100, 'Hello');
            $publisher = new Publisher($row['publisher_id'], $row['publisher_name']);
            $language = new Language($row['language_id'], $row['language_code'], $row['language_name']);
            $author = new Author($row['author_id'], $row['author_name']);

            
            if ($last_book_id != $row['book_id']) {

                $book = new Book();
                $book->set_book_id($row['book_id']);
                $book->set_title($row["title"]);
                $book->set_isbn13($row["isbn13"]);
                $book->set_language($language);
                $book->set_num_pages($row["num_pages"]);
                $book->set_publication_date($row["publication_date"]);
                $book->set_publisher($publisher);
                $book->add_author($author);
                $library->add_book($book);

                $last_book_id = $row["book_id"];
                $last_book = $book;
            } else {
                $last_book->add_author($author);
                                
            }
        }
    } 
    return $library;
}
$title = "";
echo "<div class='container my-3'>";
echo "<h2>Books by Title</h2>";
echo "<form method='POST' action='" . htmlspecialchars($_SERVER['PHP_SELF']) . "'>";

echo "<div class='row'>";

echo "<div class='col-3'>";

$title = "";
if (isset($_REQUEST["title"])){
    $title = $_REQUEST["title"];
}
echo "<input type='text' placeholder='Enter book title' name='title' value='{$title}'/>";
echo "</div>";

echo "<div class='col-2'>";
echo "<input id='btn' type='submit' name='execute' value='SEARCH' class='btn btn-primary'" . "/>";

echo "</div>";

echo "</div>";
echo "</div>";echo "</form>";
echo "</div>";


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Something posted

    if (isset($_POST['execute'])) {
        $title = $_POST['title'];
        execute($library, $_POST['title']);
    }
}

function execute(Library $library, string $title)
{
    
    $books = $library->get_books_by_name($title);
    echo "<div class='container my-3'>";

    echo "<div><span class='fw-bold'>";
    echo "Number of records: " . count($books);
    echo "</span></div>";
    
    echo "<div class='row'>";
    echo "<table id='table' class='table table-primary table-hover table-striped'>";

    echo "<tbody>";

    echo "<thead class='table-success'>";
    echo "<tr>";
    echo "<td>ID</td>";
    echo "<td>TITLE</td>";
    echo "<td>ISBN13</td>";
    echo "<td>NUM_PAGES</td>";
    echo "<td>PUBL. DATE</td>";
    echo "<td>PUBLISHER</td>";
    echo "<td>LANGUAGE</td>";
    echo "<td>AUTHOR</td>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    foreach ( $books as $book ) {
        
            echo "<tr>";
            echo "<td>";
            echo $book->get_book_id();
            echo "</td>";
            echo "<td>";
            echo $book->get_title();
            echo "</td>";
            echo "<td>";
            echo $book->get_isbn13();
            echo "</td>";
            echo "<td>";
            echo $book->get_num_pages();
            echo "</td>";
            echo "<td>";
            echo $book->get_publication_date();
            echo "</td>";
            echo "<td>";
            echo $book->get_publisher()->get_publisher_name();
            echo "</td>";
            echo "<td>";
            echo $book->get_language()->get_language_name();
            echo "</td>";
            echo "<td>";

            $authorsString = implode(", ", array_map(function ($author) {
                return $author->get_author_name(); 
            }, $book->get_authors()));

            echo $authorsString;
            echo "</td>";
        } 
        
    

    echo "</tbody>";
    echo "</table>";
    echo "</div>";
    echo "</div>";
}
//Close connection
$conn->close();