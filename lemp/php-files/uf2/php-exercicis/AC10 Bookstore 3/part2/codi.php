<?php
    $servername = "172.22.0.2";     
    $username = "root";
    $password = "1234";
    $dbname = "bookstore";
    
    function execute($title) {
        $sql = "CALL get_all_books_by_title(?)";

        $stmt = $GLOBALS['conn']->prepare($sql);
        $title = "%" . $title . "%";
        $stmt->bind_param("s", $title);
        $stmt->execute();

        $result = $stmt->get_result();

        if ($result->num_rows > 0) {

            echo "<table>";
                echo "<tr>";
                    echo "<td>ID</td>";
                    echo "<td>TITLE</td>";
                    echo "<td>ISBN13</td>";
                    echo "<td>NUM_PAGES</td>";
                    echo "<td>PUBL_DATE</td>";
                    echo "<td>PUBLISHER</td>";
                    echo "<td>LANGUAGE</td>";
                    echo "<td>AUTHOR</td>";
                echo "</tr>";
                while ($row = $result->fetch_assoc()) {
                    echo "<tr>";
                        echo "<td>" . $row["book_id"] ."</td>";
                        echo "<td>". $row["title"] ."</td>";
                        echo "<td>". $row["isbn13"] ."</td>";
                        echo "<td>". $row["num_pages"] ."</td>";
                        echo "<td>". $row["publication_date"] ."</td>";
                        echo "<td>". $row["publisher_name"] ."</td>";
                        echo "<td>". $row["language_name"] ."</td>";
                        echo "<td>". $row["author_name"] ."</td>";
                    echo "</tr>";
                } 
        }
        else {
            echo "0 resultats";
        }
            echo "</table>";
    }


    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
        die("Connexió fallida: " . $conn->connect_error);
    }

    echo "<form method='POST' action='" . htmlspecialchars($_SERVER['PHP_SELF']) . "'>";
    $title="";
    if (isset($_POST['execute'])) {
        $title = $_POST['title'];
    }

    echo "<input type='text' value='{$title}' placeholder='Enter Title' name='title' />";

    echo "<input type='submit' value='Select' name='execute' id='buton' class='buton' />";

    echo "</form>";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (isset($_POST['execute'])) {
            $title = $_POST['title'];
        execute($_POST['title']);
        }
    }
    $conn->close();
?>