<?php
require_once 'customer.php';
require_once 'country.php';
require_once 'address.php';

$servername = "172.29.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

// Crear la connexió
$conn = new mysqli($servername, $username, $password, $dbname);

// Mirar estat de la connexió
if ($conn->connect_error) {
    die("Connection failed: " . $conn -> connect_error);
} else {
    echo("Successfully connected" . "<br/>");
}

// Crear consulta
$sql = "SELECT * FROM `customer` c
ORDER BY c.customer_id;";

$result = $conn->query($sql);

echo "Number of records: " . $result->num_rows . "<br>";

echo "<table class='table'>";
    echo "<tr>";
        echo "<td>customer_id</td>";
        echo "<td>first_name</td>";
        echo "<td>last_name</td>";
        echo "<td>email</td>";
    echo "</tr>";

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $customer = new Customer($row["customer_id"], $row["first_name"], $row["last_name"], $row["email"]);
            
            echo "<tr>";
                echo "<td>" . $address->get_customer_id() . "</td>";
                echo "<td>" . $address->get_first_name() . "</td>";
                echo "<td>" . $address->get_last_name() . "</td>";
                echo "<td>" . $address->get_email() . "</td>";
            echo "</tr>";
        }
    }
echo "</table>";  

$conn->close();

?>