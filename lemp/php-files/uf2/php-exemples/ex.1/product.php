<?php 
    class Product
    {
        // Properties or Attributes
        private int $id;
        private string $name;
        private float $price;
        private array $color;

        // Constructor
        function __construct(int $id, string $name, float $price, array $color){
            $this->id = $id;
            $this->name = $name;
            $this->price = $price;
            $this->color = $color;
        }

        // Destructor (optional)
        function __destruct(){
            // unset($this); 
            #echo "Destruction of {$this->name}." . "<br>";
        }

        // Getters and setters
        function get_id(): int {
            return $this->id;
        }

        function set_id(int $id): void {
            $this->id = $id;
        }

        function get_name(): string {
            return $this->name;
        }

        function set_name(string $name): void {
            $this->name = $name;
        }

        function get_price(): float{
            return $this->price;
        }

        function set_price(float $price): void{
            $this->price = $price;
        }

        function get_color(): array {
            return $this->color;
        }

        function set_color(array $color): void{
            $this->color = $color;
        }

        // Methods
        function tax(float $tax): float {
            return $this -> price * $tax / (1 + $tax);
        }

        function priceNoTax(float $tax): float{
            return $this-> price/(1+$tax);
        }



        // toString
        function __toString(): string {
            $text="";
            foreach ($this->color as $color){
                $text .= $color . " ";
            }
            return "Product[id=" . $this->id . ", name" . $this->name .  ", price=" . $this-> price . ", colors= " . $text . "]";
        }
    }

    $p1 = new Product(1, "basic T-shirt", 12.55, ["Red", "Blue", "Green"]);
    $p2 = new Product(2, "Long T-shirt", 18.75, ["Black"]);

    echo $p1->get_id() . "<br />";
    echo $p1->get_name() . "<br />";
    echo $p1->get_price() . "<br />";
    $array_colors = $p1->get_color();
    print_r($array_colors);
    //var_dump($p2->get_colors())

?>
    <?php
    echo "<br><br>";
    echo "Setters: <br />";
    $p1 -> set_id("10");
    echo $p1 -> get_id() . "<br />";

    $p1 -> set_price("15.75");
    echo $p1 -> get_price() . "<br />";
?>

<?php 
    echo "<br><br>";
    echo "Methods: <br />";
    echo $p1 -> priceNoTax(0.21)  . "<br />";
    echo $p1 -> tax(0.21) . " <br />";

    echo $p1->__toString();
?>

<?php
    echo "Product Array: <br>";
    $products=[];
    $products[]=$p1; //array_push($products,$p1)
    $products[]=$p2; 

    foreach ($products as $product){
        echo $product->__toString() . "<br />";
    }
?>
