<?php
$servername = "172.22.0.2";     //docker inspect lemp-mariadb-1
$username = "root";
$password = "1234";
$dbname = "bookstore";


// Using MySQLi Object-Oriented

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("" . $conn->connect_error);
} else {
    //OK
    echo "DB successfull connected!" . "<br />" . "<br />";
}


// SELECTS

$sql = "SELECT book_id, title, isbn13, num_pages FROM book  WHERE title LIKE '%?%' ORDER BY title;";
$sql2 = "SELECT book_id, title, isbn13, num_pages FROM book  WHERE book_id = ? AND title = ? ORDER BY title";
$sql3 = "SELECT book_id, title, isbn13, num_pages FROM book  WHERE book_id = ? AND title LIKE LOWER(?) ORDER BY title";

$injection = "INSERT INTO book (book_id, title, isbn13, language_id, num_pages, publication_date, publisher_id FROM book  
WHERE book_id = ? AND title LIKE LOWER(?) ORDER BY title)) VALUES (?, ?, ?, ?, ?, ?, ?)"; 

$stmt = $conn->prepare($injection);
$stmt->bind_param("issiisi", $id, $title, $isbn, $language_id, $num_pages, $publication_date, $publisher_id); # i -> Integer ($id), s -> String ($title)
$id = 11131;
$title = "New Book";
$isbn = "1111111";
$language_id = 1;
$num_pages = 100; 
$publication_date = "2023-05-03";
$publisher_id = 1;

$stmt->execute();

# $result = $stmt->get_result();

if ($stmt->affected_rows > 0) {
        echo "Num records: " . $stmt->affected_rows . "<br />";
} else {
    echo "NO DATA FOUND";
}

//Close connection
echo "<br />" . "Disconnect";
$conn->close();



?>