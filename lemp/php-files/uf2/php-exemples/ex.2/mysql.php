<?php
$servername = "172.22.0.2";     //docker inspect lemp-mariadb-1
$username = "root";
$password = "1234";
$dbname = "bookstore";


// Using MySQLi Object-Oriented

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("" . $conn->connect_error);
} else {
    //OK
    echo "DB successfull connected!" . "<br />" . "<br />";
}


// SELECTS

$sql = "SELECT book_id, title, isbn13, num_pages FROM book ORDER BY 3;";

$result = $conn->query($sql);

if ( $result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        echo $row["book_id"] ." ". $row["title"] . " ". $row["isbn13"] ." ". $row["num_pages"] . "<br />";
    }
} else {
    echo "NO DATA FOUNDDDDD!!!";
}


//Close connection
echo "<br />" . "Disconnect";
$conn->close();



?>