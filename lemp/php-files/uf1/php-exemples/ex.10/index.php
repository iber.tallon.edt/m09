<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport">
    <title>JSON</title>
</head>
<body>
    <!--Per poder crear json gratuitament: https://api.jsonserve.com/  -->
    <?php
        $age = array("Peter" => 35, "Ben" => 37, "Joe" => 43);
        $json = json_encode($age);

        var_dump($json);

        echo "<br />" . "<br />";

        # ------------------------------------------------------------------------
        # PHP - json_encode()

        $jsonobj = '{"Peter":35,"Ben":37,"Joe":43}';

        $array2 = json_decode($jsonobj);

        var_dump($array2);

        echo "<br />" . "<br />";
    
        # ------------------------------------------------------------------------
        # PHP - json_decode()

        echo $array2->Peter . "<br />";
        echo $array2->Ben . "<br />";
        echo $array2->Joe . "<br />";
        echo "<br />";


        $array3 = json_decode($jsonobj, true);

        var_dump($array3);

        echo $array3["Peter"] . "<br />";
        echo $array3["Ben"] . "<br />";
        echo $array3["Joe"] . "<br />";

        # ------------------------------------------------------------------------
        # Using file_get_contents() and URL

        echo "<br />" . "<br />";
        $url = "https://jsonplaceholder.typicode.com/posts/1/";
        $data = file_get_contents($url);
        $obj2 = json_decode($data); // decode the JSON feed
        
        foreach ($obj2 as $key => $value) {
            echo $key . "=>" . $value . "<br>";
        }

        # ------------------------------------------------------------------------
        # Object with array

        // {"contacts":[{"name": "John", "age": 25}, {"name": "Jane", "age": 30}]}
        
        echo "<br />" . "<br />";
        $url = 'data.json'; # path to your JSON file
        $data = file_get_contents($url); # put the contents of the file into a variable
        $obj3 = json_decode($data); # decode the JSON feed

        #First register
        $obj4 = $obj3->contacts[0];
        #Iterating first object $obj2
        foreach ($obj4 as $key => $value) {
            echo "[" . $key . ", " . $value . "]" . "<br>";
        }

        echo '<br />';
        #Iterating all $obj
        $obj3 = json_decode($data, true); // decode the JSON feed

        foreach ($obj3["contacts"] as $contact) {
            foreach ($contact as $key => $value) {

                echo "[" . $key . ", " . $value . "]" . "<br>";
            }
        }

        echo '<br />';
        #Get a specified key-value
        foreach ($obj3["contacts"] as $contact) {

            echo $contact['name'] . "<br>";
            echo $contact['age'] . "<br>";
    }


    ?>


</body>
</html>