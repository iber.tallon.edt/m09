<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport">
        <title>Forms</title>
    </head>
    <body>
        <form action=
            "<?php 
                echo htmlspecialchars($_SERVER["PHP_SELF"])
            ?>" method="post">
            <label for="name">Name:</label><br />
            <input id="name" type="text" name="name" /><br />
            <label for="email">Email:</label><br />
            <input id=email type="text" name="email"><br /><br />
            <input type="submit" name="execute">
        </form>

        <?php

            function validate($data) {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }
            if ($_POST) {
                if(isset($_POST["execute"])) {
                    execute();
                }
            }

            function execute () {
                if(isset($_REQUEST["name"]))
                echo "Name: " . validate($_REQUEST['name']) . "<br />";

                if(isset($_REQUEST["email"])) 
                echo "Email: " . validate($_REQUEST['email']) . "<br />";
            }

        ?>
    </body>
</html> 