<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport">
        <title>Forms</title>
    </head>
    <body>
        <?php

            function validate($data) {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }
            if ($_POST) {
                if(isset($_POST["execute"])) {
                    execute();
                }
            }

            function execute () {
                if(isset($_REQUEST["name"]))
                echo "Name: " . validate($_REQUEST['name']) . "<br />";

                if(isset($_REQUEST["email"])) 
                echo "Email: " . validate($_REQUEST['email']) . "<br />";
            }

        ?>
    </body>
</html> 