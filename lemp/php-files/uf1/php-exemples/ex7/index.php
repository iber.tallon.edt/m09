<?php
$bol1 = TRUE;
$bol2 = FALSE;

echo "bol1 = $bol1" . "<br />";
echo "bol2 = $bol2" . "<br />";

var_dump($bol1);
var_dump($bol2);

if ($bol1) {
    echo "bol1 is true" . "<br />";
}else {
    echo "bol1 is false" . "<br />";
}

if ($bol2) {
    echo "bol2 is true" . "<br />";
}else {
    echo "bol2 is false" . "<br />";
}

// -----------------------------------------------------------------------------------

// Operadors

    echo "<br>";
    //declare(strict_types=1); // requeriment estricte
    function add(float $a,float $b) : float
    {
        return $a + $b;
    }
    echo "Add: " . add(5.5, 5.3);
    echo "<br />";
    // since strict is NOT enabled "5" is changed to int(5), and it will return 10

// -----------------------------------------------------------------------------------

// COnditionals

    $h = date("H");
    $y = date("Y");
    $d = date("Y/m/d");
    echo $h . "<br>";
    echo $y . "<br>";
    echo $d . "<br>";

    if ($h < "10") {
        echo "Have a good morning!" . "<br />";
        echo "<br />";
    } elseif ($h < "20") {
        echo "Have a good day!" . "<br />";
    } else {
        echo "Have a good night!" . "<br />";
    }

    $fvcolor = "red";

    switch ($fvcolor) {
        case "red":
        echo "Your favorite color is red";
        break;
        case "blue":
        echo "Your favorite color is red";
        break;
        case "green":
        echo "Your favorite color is green";
        break;
        default:
        echo "Your favorite color is neither red, blue or green";
    } 

    echo "<br />" . "<br />";


  // -----------------------------------------------------------------------------------

// Loop (bucles)

    $number = 1;

    while ($number <= 5) {
        echo "The number is: $number <br>";
        $number++;
    }
    echo "<br />" . "<br />";

    for ($number = 0; $number <= 10; $number++) {
        echo "The number is: " . $number . "<br />";
    }

    echo "<br />" . "<br />";

  // -----------------------------------------------------------------------------------

// Array

    $array1 = array("RED", "BLUE", "GREEN");

    $array2 = ["CYAN", "YELLOW", "MAGENTA", "BLACK"];

    echo $array1[0] . "<br />";
    echo $array1[1] . "<br />";
    echo $array1[2] . "<br />";

    $len = count($array1);
    
    for ($i=0; $i < count($array1); $i++) { 
        echo $array1[$i] . " ";
    }


    $games = array(
        "Playstation 2" => "RED",
        "Game Boy" => "GREEN",
        "WII" => "BLUE",
    );
    echo "<br />";

    echo $games["Game Boy"] . "<br />";
    // echo $games[1] . "<br />"; Dona ERROR
    echo "<br />";

    foreach ($games as $key => $value) {
        echo $key . ": " . $value . "<br />";
    }

    echo "<br />" . "<br />";

    //multi (taules)

    $cars = array (
        array("Volvo",22,18),
        array("BMW",15,13),
        array("Saab",5,2),
        array("Land Rover",17,15)
      );
      echo $cars[0][0] . ": In stock: " . $cars[0][1] . ", sold: " . $cars[0][2] . ".<br>";
      echo $cars[1][0] . ": In stock: " . $cars[1][1] . ", sold: " . $cars[1][2] . ".<br>";
      echo $cars[2][0] . ": In stock: " . $cars[2][1] . ", sold: " . $cars[2][2] . ".<br>";
      echo $cars[3][0] . ": In stock: " . $cars[3][1] . ", sold: " . $cars[3][2] . ".<br>";

      echo "<br />" . "<br />";

      for ($row = 0; $row < 4; $row++) {
        echo "<p><b>Row number $row</b></p>";
        echo "<ul>";
        for ($col = 0; $col < 3; $col++) {
            echo "<li>" . $cars[$row][$col] . "</li>";
        }
        echo "</ul>";
    }
    

?>