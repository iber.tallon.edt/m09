<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Solutions</title>
</head>
<body>

<?php
    $h1 = "Title Page";
    $txt = "<h1>$h1</h1>";
    echo $txt;

    $p = "This is a very silly line with a \$txt";
    echo "<p>$p</p>";

    function myFunction1() {
        $div = "This is a div";
        echo "<div>$div</div>";
    }

    myFunction1();
?>
</body>
</html>