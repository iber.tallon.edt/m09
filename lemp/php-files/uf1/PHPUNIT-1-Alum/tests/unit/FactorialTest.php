<?php 
declare(strict_types=1);

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

final class FactorialTest extends TestCase {

   
//8. Binary
#[Test]
#[TestDox("Facotrial")]
public function testFactorial() {

    require_once __DIR__ . "/../../src/php/Factorial.php";

    $fac = new Factorial();

    $this->assertSame(120, $fac->factorial(5));
    $this->assertSame(6, $fac->factorial(3));
    $this->assertSame(2, $fac->factorial(2));
    $this->assertSame(1, $fac->factorial(1));
    $this->assertSame(5040, $fac->factorial(7));
    $this->assertSame("\$num can not be negative", $fac->factorial(-7));
  }


}
    
