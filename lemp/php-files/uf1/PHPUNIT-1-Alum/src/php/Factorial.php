<?php
class Factorial
{

  /*
  Calcula el factorial d'un nombre.

  La fórmula per calcular el factorial és:

  n! = n × (n-1) × (n-2) × … × 1

 
  El factorial de zero és 1: 
  
  0! = 1

  Exemples:
  factorial(5) = 120
  factorial(7) = 5040
  factorial(0) = 1
  factorial(3) = 6
  factorial(-7) = "$num can not be negative"

  Executar proves:
  Obrir terminal.
  Siturar-se al directori "tests".
  Executar el test unitari: 
  php phpunit.phar --testdox .\unit\FactorialTest.php
  */

  public function factorial(int $num): int | string
  {
    if ($num < 0) {
      return "\$num can not be negative";
    }
    $fac = 1;
    for ($i = 1; $i <= $num; $i++) {

      $fac *= $i;
    }
    return $fac;
  }
}
