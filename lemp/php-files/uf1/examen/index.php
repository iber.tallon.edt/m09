<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css" />
    <title>Examen PHP</title>
</head>
<body>
    <h1>Animes</h1>



    <?php
        $url = "https://joanseculi.com/edt69/animes3.php"; // path to your JSON file
        $data = file_get_contents($url); // put the contents of the file into a variable
        $array = json_decode($data, true); // decode the JSON feed
        $num_id = $array['animes'][0][0];

        echo "<div>";
            echo " Num animes: " . "$num_id";
        echo "</div>";

        foreach ($array['animes'] as $anime) {            

            echo "<table>";
                echo "<tr>";

                    $image = "https://joanseculi.com/" . $anime['image'];
                            echo "<tr>";
                                echo '<td><img src="' . $image . '" alt="' . $anime['name'] . '"></td>';
                            echo "</tr>";
                            echo "<tr>";
                                echo "<td>" . "<span>ID: </span>" . $anime['id'] . "</td>";
                                echo "<td>" . "<span>Type: </span>" . $anime['type'] . "</td>";
                            echo "</tr>";
                            echo "<tr>";
                                echo "<td>" . "<span>Name: </span>" . $anime['name'] . "</td>";
                                echo "<td>" . "<span>Year: </span>" . $anime['year'] . "</td>";
                            echo "</tr>";
                            echo "<tr>";
                                echo "<td>" . "<span>Original Name: </span>" . $anime['originalname'] . "</td>";
                                echo "<td>" . "<span>Rating: </span>" . $anime['rating'] . "</td>";
                            echo "</tr>";
                            echo "<tr>";
                                echo "<td>" . "<span>Demography: </span>" . $anime['demography'] . "</td>";
                                echo "<td>" . "<span>Genre: </span>" . $anime['genre'] . "</td>";
                            echo "</tr>";
                                echo "<td>" . "<span>Synopsis: </span>" . "<br />" . "<br />" . $anime['description'] . "</td>";
                            echo "</tr>";
                echo "</tr>";
            echo "</table>";
        }        
    ?>
</body>
</html>
