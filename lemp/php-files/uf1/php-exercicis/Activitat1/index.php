<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activitat 1</title>
    <link rel="stylesheet" type="text/css" href="Graphic.css">
</head>
<body>
    <div class='container'>
        <?php
            echo "<h1 style='color:black;'>Part1</h1>";
            include "Graphic.php";
            echo "<br />" . "<br />";
            echo "<h1 style='color:black;'>Part2</h1>";
            include "Graphic2.php";
        ?>
    </div> 
</body>
</html>
