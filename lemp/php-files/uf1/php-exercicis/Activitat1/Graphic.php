<?php

  $rutaImage = './' . 'green.png';

  $consoles = array (
      array("Playstation 2",155),
      array("Nintendo DS",154),
      array("Game Boy",119),
      array("Play Station 4",102),
      array("Wii",101),
      array("Play Station 3",87),
      array("Xbox 360",84),
      array("Play Station Portable",82),
      array("Game Boy Advance",81),
      array("Nintendo 3DS",72),
      array("Nes",62),
      array("Nintendo Switch",60)
    );
  
  $limit_max = 100;
  echo "<div class='container1'>";
  echo "<h1>VIDEO GAMES CONSOLES</h1>";
  echo "<h2>Best Selling video Games Consoles 1983-2024</h2>";
  echo "<table>";
  echo "<tr><th>Consola</th><th>Barra</th><th>Vendes</th></tr>";

  // Mostrar dades
  foreach ($consoles as $consola) {
      echo "<tr>";
      echo "<td>" . $consola[0] . " : " . "</td>";
      
      echo "<td>";
      $barra = min($consola[1] * 2, $limit_max);
      
      echo str_repeat('<img src="green.png" alt="green" >', $consola[1]);    
      echo "</td>";
      
      echo "<td>" . $consola[1] . ' Millions' . "</td>";
      
      echo "</tr>";
  }

  echo "</table>";

  echo "<p style='padding-left: 10px; padding-bottom:10px; padding-top: 10px; font-size:12px;'>© Iber Tallón</p>";
  echo "</div>";
?>
