<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Activitat 4</title>
        <link rel="stylesheet" type="text/css" href="tax.css">
        <style>
            .error {color:#FF0000;}
        </style>
    </head>
    <body>
        <?php
            $price = $percent = "";
            $numErr = $numtaxErr = "";
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $name = validate($_POST["price"]);
                $email = validate($_POST["percent"]);
            }

            function validate($data) {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }

            function calc_iva($iva, $percent_iva) {
                // Calcula el IVA tenint en compta la taxa(IVA) i el tant per cent de la taxa(IVA) Utiliza la función is_numeric para comprobar si el valor es un número
                $total_iva = ($iva * 100) / ($percent_iva + 100);
                return $total_iva;
            }
            
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $price = $_POST["price"];
                if (!preg_match('/^-?(?:\d+|\d*\.\d+)$/', $price)) {
                    $numErr = "Please enter a number.";
                    print($numErr);
                } 
                #else {
                #    $price = validate($_POST["price"]);
                #}
            }

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $percent = $_POST["percent"];
                if ($percent > 100 or $percent < 0) {
                    $numtaxErr = "Please enter a valid Tax number.";
                } 
                #else {
                #    $percent = validate($_POST["percent"]);
                #}
            }

        ?>
        <h1>PRICE, TAX and ROUNDS</h1>
        <form method='post' action='<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>'>
            Price with TAX:<br />
            <input type="text" name="price" id="price" required/>
            <span class="error">
                * <?php echo $numErr;?>
            </span>
            <br />
            <br />
            TAX (%): <br /> 
            <input type="text" name="percent" required />
            <span class="error">
                * <?php echo $numtaxErr;?>
            </span>
            <br />
            <br />
            <br />
            <input type="submit" name="submit" value="calculate" class="calculate" />
            <br />
        </form>

        <?php
            echo "<h1>TAX data:</h1>";
            echo "Price without tax: " . calc_iva($price, $percent) . " €";
            echo "<br>";
            echo "Round to 2 decimals using round(): " . round(calc_iva($price, $percent), 2) . " €";
            echo "<br>";
            $format = sprintf("%01.4f" ,calc_iva($price, $percent)); 
            echo "Round to 4 decimals using function SPRINTF: " . $format . " €";
        ?>
    </body>
</html>