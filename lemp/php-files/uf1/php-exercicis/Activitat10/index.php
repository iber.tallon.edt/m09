<!DOCTYPE html>
<html lang="en">
<head>
    <title>Activitat 10</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
    <h2 class="text-center mt-4">ROLL A NUMBER OF DICE</h2>
    <?php include 'dice.php'?>
    <div class="ms-5">
        <form method="POST">
            <label style="padding-bottom:10px">Number: </label>
            <br/>
            <div class="col-md-1">
                <input class="form-control" type="num" name="num" id="num" required
                value="<?php echo isset($_POST['num']) ? $_POST['num'] : ''; ?>">   
            </div>
            <br/>
            <div class="mt-3">
                <button type="submit" name="execute" class="btn btn-primary">ROLL</button>
            </div>
            <p style="margin-top: 5px;">Click the button to roll a number of dice.</p>
        </form>
        <p><?php echo execute();?></p>
    </div>
</body>
</html>
