<?php

function execute()
{
    $num = "";
    $resultat = "";
    if (isset($_POST['execute'])){
        if (isset($_POST['num']) && $_POST['num'] != ""){
            $num = validate($_POST['num']);
            $resultat = array_usuari($num); 
        }
    }
    return $resultat;
}

function images($img){
    foreach ($img as $image){
        echo $image;
    }
}

function validate($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function array_usuari($num_users){
    $total = 0;
    $valors = "";
    $dados = [[1,'<img src="images/image1.png">'],
                [2,'<img src="images/image2.png">'], 
                [3,'<img src="images/image3.png">'],
                [4,'<img src="images/image4.png">'],
                [5,'<img src="images/image5.png">'],
                [6,'<img src="images/image6.png">']];
    $array = []; 
    
    for ($i = 0; $i < $num_users; $i++){
        $random = rand(0,5);
        $array[] = $dados[$random][1];
        $valors .= " " . $dados[$random][0];
        $total += $dados[$random][0];
        
    }

    echo "<h3>". "Rolling " . $num_users . " dice" . "</h3>";
    echo images($array);
    echo "<br><br/>";
    echo "<h3>Result</h3>";
    echo "The values are: " . $valors . "<br>";
    echo "Total: " . $total . "<br/>";

    for ($i = 1; $i <= 6; $i++) {
        $comptador = 0;
        foreach ($array as $value) {
            if ($value == '<img src="imagen' . $i . '.png">') {
                $comptador++;
            }
        }
    }
}

?>
