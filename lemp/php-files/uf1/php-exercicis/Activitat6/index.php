<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Activitat 5</title>
        <link rel="stylesheet" type="text/css" href="factorial.css">
    </head>
    <body>
        <?php

            function execute() {
                # Funció encarregada de mostrar el resultat final.
                
                $numero = "";

                # Fa una validació del valor s'ha introduit en el formulari.                 
                if ($_SERVER["REQUEST_METHOD"] == "POST") { 
                    $numero = validate($_POST["num"]);
                }

                # Processa el número per la funció "factorial()" per calcular el seu factorial.
                return factorial($numero);  
            }

            function validate($data){
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }

            function factorial($val):float {
            # Funció que calcula el factorial d'un número.
                
                # Quan el número introduit és 0, el valor returnat és 1.
                if ($val == 0) {
                    return 1;
                }

            # Es poden calcular de dos formes:
                
            ### Forma 1
            
                # Aquí es fa una funció recursiva.
                # Aquesta té dos etapes: la trucada inicial i la tornada de la trucada recursiva.
                # Ex: factorial(5)

                # Trucada inicial:
                # 1. La funció es truca a si mateixa amb $val - 1. Llavors, truca a factorial(4). 
                # 2. Així successivament fins arribar a factorial(1).
                
                # Tornada de la trucada recursiva: 
                # 1. La funció factorial(2) rep el  valor retornat per la trucada a factorial(1), que es 1. 
                # 2. Multiplica $val (2) por el valor returnat per factorial 1 (1), on com a resultat de la multiplicació dona 2.
                # 3. Això es fa successivament fins arribar a factorial(5).
               
                #return $val * factorial($val - 1);
            
            ### Forma 2

                # Aquí es crea una variable que anirà incrementant de 1 en 1 fins arribar al mateix nombre que $val. una funció recursiva.
                $factorial = 1;
                
                # Multiplica tots els números des de 1 fins a $val per poder obtindre el seu factorial.
                for ($i = 1; $i <= $val; $i++) {
                    $factorial *= $i;
                }
                
                return $factorial;
            }
            

            function factorialTable(){
                //for per generar table
                echo "<table class='factorial'>";
                echo "<tr>"; 
                    echo "<th class='factorial'>n</th>";
                    echo "<th class='factorial'>n!</th>";
                echo "</tr>";
                for ($i=0; $i<=100; $i++){
                    echo "<tr>";
                        echo "<td class='factorial'>" . $i . "</td>";
                        echo "<td class='factorial'>" . factorial($i) . "</td>";
                    echo "</tr>";
                }
        
        
            echo "</table>";

            }
        ?>
        <div class="color_fons">
            <h1>Factorial Calculator: n!</h1>
            <div class="container">
                <!--$_SERVER["PHP_SELF"] l'utilitzo per a especificar que l'informació que ha d'agafar el formulari està en aquest mateix fitxer i no en un extern.-->
                <form method="post" action="<?php echo $_SERVER["PHP_SELF"]?>"> 
                    <p>Enter a number to calculate the factorial n! using the calculator below</p>
                    <label for="number">Number: </label>
                    <input class="number" type="number" name="num" id="number" placeholder="Enter a number" value="<?php echo $num;?>" /><br /><br />
                    <input class="bottom" type="submit" value="SEND" /><br />
                </form>
                <div class="table_grey">
                    <div class="resultat">Factorial Number: </div><br />
                    <div class="resultat"> 
                        n! = <span id="resultat"><?php echo execute();?></span>
                    </div>
                </div>
            </div>
            <br /><br />
            <h2>Factorial Formules</h2>
                The formula to calculate a factorial for a number is:<br /><br />
                n! = n × (n-1) × (n-2) × ... × 1<br /><br />
                Thus, the factorial n! is equal to n times n minus 1, times n minus 2, continuing until the
                number 1 is reached.<br /><br />
                The factorial of zero is 1:
                0! = 1<br /><br />
            
            <h2>Recurrence Relation</h2>
                And the formula expressed using the recurrence relation looks like this:<br /><br />
                n! = n × (n – 1)!<br /><br />
                So the factorial n! is equal to the number n times the factorial of n minus one. This recurses
                until n is equal to 0.<br /><br />

            <h2>Factorial Table</h2>
            The table below shows the factorial n! for the numbers one to one-hundred.<br /><br />
            <?php 
                echo factorialTable()
            ?>
        </div>
        <?php

        ?>
    </body>
</html>