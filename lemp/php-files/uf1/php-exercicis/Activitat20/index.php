<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport">
    <title>JSON</title>
    <link rel="stylesheet" type="text/css" href="peak.css">
</head>
<body>
    <h1>3000+ Peaks</h1>
    <!--Per poder crear json gratuitament: https://api.jsonserve.com/  -->
    <?php
        $url = "https://api.jsonserve.com/Gk1Go7"; // path to your JSON file
        $data = file_get_contents($url); // put the contents of the file into a variable
        $obj = json_decode($data); // decode the JSON feed
        
     //   var_dump($obj);

        echo "<table>";
            foreach($obj as $peak) {
                $image=json_encode($peak->url);
                echo "<tr>";

                    echo "<td class='img'>" .
                
                        "<img src=" . $image . " alt=Imagen" .
                    
                    "</td>";
                    echo "<td class='data'>";
                    echo "<div>";
                        echo "<p class='name'>" . "Name: " . $peak->name . "</p>"; 
                        echo "<p>" . "Height: " . $peak->height . "</p>"; 
                        echo "<p>" . "Prominence: " . $peak->prominence . "</p>";
                        echo "<p>" . "Zone: " . $peak->zone . "</p>";
                        echo "<p>" . "Country" . $peak->country . "</p>"; 
                    echo "</div>";
                    echo "</td>";            
                echo "</tr>";
            }
        echo "</table>";
    ?>
</body>
</html>

       