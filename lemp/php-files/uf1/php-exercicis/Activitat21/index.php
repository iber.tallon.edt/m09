<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
    <meta name="viewport">
    <title>JSON Sports</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

  <?php

  $url = "https://www.thesportsdb.com/api/v1/json/3/all_countries.php"; 

  $dades = file_get_contents($url); 
  $paisos = json_decode($dades, true);

  echo "<div class='container mt-3'>";
      echo "<form method='POST' action='" . htmlspecialchars($_SERVER['PHP_SELF']) . "'>";
    
          echo "<div class='row'>";
              echo "<div class='col-md-6'>";
                  echo "<select name='pais' class='form-control'>";
                  foreach ($paisos["countries"] as $value) {
                      $pais = $value['name_en']; 
                      $selected = isset($_POST['pais']) && $_POST['pais'] === $pais ? ' selected' : '';
                      echo "<option value='$pais' $selected>$pais</option>";
                  }
                  echo "</select>";
              echo "</div>";
              echo "<div class='col-md-6'>";
                  echo "<input type='submit' name='execute' value='Select' class='btn btn-primary'>";
              echo "</div>";
          echo "</div>";
          
      echo "</form>";

  function validate($data)
  {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
  }

  if ($_SERVER["REQUEST_METHOD"] == "POST" and isset($_POST['execute'])) {
    if (isset($_POST['pais']) && $_POST['pais'] != "") {
        $pais = validate($_POST['pais']);
        execute($pais);
    }
  }

  function execute($pais)
  {
      $url = "https://www.thesportsdb.com/api/v1/json/3/search_all_leagues.php?c=" . $pais; 
      echo "<br>"; 
      echo "<table class='table table-color' >";
          echo "<thead class='table table-primary' style='text-align: center'>";
              echo "<th >Flag</th>";
              echo "<th>Title</th>";
              echo "<th>Website</th>";
              echo "<th>Image</th>";
          echo "</thead>";
          echo "<tbody style='text-align: center' class='custom'>";


          $dades = file_get_contents($url); 
          $paisos = json_decode($dades, true);
          if (isset($paisos["countries"]) && $paisos["countries"] !== null) {
              foreach ($paisos["countries"] as $valor) {
          
                  $flag = "<img width='100px' src='" . $valor['strBadge'] . "'><br>";            
                  $title = $valor['strLeague'] . "<br>"; 
                  $website = "";
                  if (isset($valor['strWebsite'])) {
                      $website = "<a href='https://" . $valor['strWebsite'] . "'>" . $valor['strWebsite'] . "</a><br>";
                  }
                  $image = "";
                  if (isset($valor['strFanart1'])) {
                      $image = "<a href='" . $valor['strFanart1'] . "'><img class='rounded'  width='300px' src='" . $valor['strFanart1'] . "'></a><br>";

                  }
                  $description = $valor['strDescriptionEN'] . "<br>"; 
                  echo "<tr'>";
                      echo "<td class='flag'>$flag</td>";
                      echo "<td >$title</td>";
                      echo "<td>$website</td>";
                      echo "<td>$image</td>";
                  echo "</tr>";
                  echo "<tr'>";
                      echo "<td  style='text-align: justify;' colspan='4'>$description</td>";
                  echo "</tr>";

              }
          }
          echo "</tbody>";
      echo "</table>";

  }

  ?>

</body>
</html>