<?php

  # len de la primera taula
  $len1 = 30;
  # array amb números al atzar (el tamany dependrà del número que tingui la variable "len").
  $numrandom = array();

  # bucle que crea l'array amb números al atzar.
  for ($i = 0; $i < $len1; $i++) {
   $numrandom1[$i] = rand(100, 999);
  }

  echo "<h1>TAULA 1</h1>";
  echo "<table class='taulablue'>";
    
    echo "<tr>";
      for ($i = 0; $i < $len1; $i++) {
        echo "<th style='font-weight: bold;'>$i</th>";
      }
    echo "</tr>";
    
    echo "<tr>";
      for ($i = 0; $i < $len1; $i++) {
        echo "<td>$numrandom1[$i]</td>";
      }      
    echo "</tr>";    

  echo "</table>";

  echo "<br />";

# ------------------------------------------------------------------------------------------------------------------------

  # len de la primera taula
  $len2 = 20;


  echo "<h1>TAULA 2</h1>";
  echo "<table class='taulablue'>";
    
    echo "<tr>";
      for ($i = 0; $i < $len2; $i++) {
        echo "<th style='font-weight: bold;'>$i</th>";
      }
    echo "</tr>";
    
    echo "<tr>";
      for ($i = 0; $i < $len2; $i++) {
        echo "<td>$numrandom1[$i]</td>";
      }      
    echo "</tr>";    

  echo "</table>";

  echo "<br />";

# ---------------------------------------------------------------------------------------------------------------------

# Funció que pren un número com a argument i retorna 'true' si es par i 'false' si es impar.
function esPar($numero) {
    return $numero % 2 == 0;
}

for ($i = 0; $i < $len2; $i++) {
  $numrandom2[$i] = rand(100, 999);
 }
# Separa els números pareis e imapreis.
# Posa els números parells dins de la variable "array_par".
$array_par = array_filter($numrandom2, "esPar");
$array_impar = array_filter($numrandom2, function($numero) {
    return !esPar($numero);
});

sort($array_par);
sort($array_impar);

// Imprimir la tabla con los números ordenados
echo "<h1>TAULA 3</h1>";
echo "<table class='taulagreen'>";

  // Encabezado de la tabla
  echo "<tr>";
    for ($i = 0; $i < $len2; $i++) {
        echo "<th style='font-weight: bold;'>$i</th>";
    }
  echo "</tr>";

  // Contenido de la tabla
  echo "<tr>";
    for ($i = 0; $i < count($numrandom2); $i++) {
      if ($i < count($array_par)) {
          echo "<td>{$array_par[$i]}</td>";
      } else {
          echo "<td>{$array_impar[$i - count($array_par)]}</td>";
      }
    }
  echo "</tr>";

echo "</table>";

echo "<br />" . "<br />";

echo "<h1>SUMES</h1>";

$suma_Parells = 0;
$suma_Imparells = 0;
$suma_Total = 0;

for ($i = 0; $i < $len2; $i++) {
  $suma_Total += $numrandom2[$i];
  if ($numrandom2[$i] % 2 == 0) {
    $suma_Parells += $numrandom2[$i];
  }
  else {
    $suma_Imparells += $numrandom2[$i];
  }
}

echo "<br />";
echo "<div style='margin: auto;width: 50%;'>";
  echo "<table style='text-align:start;'>";
  echo"<tr><td style='font-size: 20px; padding-left:1px;'>Total Evens</td></tr>";
    echo"<tr><td style='border: 1px solid black; padding: 5px; border-radius: 7px; border-color: blue;'>$suma_Parells</td></tr>";
    echo"<tr><td style='font-size: 20px; padding-left:1px;'>Total Odds</td></tr>";
    echo"<tr><td style='border: 1px solid black; padding: 5px; border-radius: 7px; border-color: blue;'>$suma_Imparells</td></tr>";
    echo"<tr><td style='font-size: 20px; padding-left:1px;'>Total Evens and Odds</td></tr>";
    echo"<tr><td style='border: 1px solid black; padding: 5px; border-radius: 7px; border-color: blue;'>$suma_Total</td></tr>";
  echo "</table>";
echo "</div>";
echo "<br />" . "<br />";

# ---------------------------------------------------------------------------------------------------------------------

echo "<h1>Average</h1>";

$avg = $suma_Total/count($numrandom2);

echo "<br />";
echo "<div style='margin: auto;width: 50%;'>";
  echo "<table style='text-align:start;'>";
    echo"<tr><td style='font-size: 20px; padding-left:1px;'>Total Average</td></tr>";
    echo"<tr><td style='border: 1px solid black; padding: 5px; border-radius: 7px; border-color: blue;'>$avg</td></tr>";
  echo "</table>";
  echo "</div>";
echo "<br />" . "<br />";

# ---------------------------------------------------------------------------------------------------------------------

echo "<h1>Array multiples de 10</h1>";

$num_array_mult_10 = 0;
$num_mult_10 = 0;

echo "<div style='margin: auto;width: 50%;'>";
  echo "<table style='border-collapse: collapse;'>";
    
    echo"<p style='font-size: 20px; padding-left:1px;'>Multiples of 10</p>";

    echo "<tr>";
      for ($i = 0; $i < $len2; $i++) {
        if ($numrandom2[$i] % 10 == 0) {
          $num_array_mult_10=$num_array_mult_10 +1;
          echo "<th style='font-weight: bold; background-color: rgb(222, 242, 194); padding-left: 10px;'>$num_array_mult_10</th>";
        }
      }
    echo "</tr>";
    
    echo "<tr>";
      for ($i = 0; $i < $len2; $i++) {
        if ($numrandom2[$i] % 10 == 0)
          echo "<td style='background-color: rgb(222, 242, 194); padding-left: 10px;'>$numrandom2[$i]</td>";
      }      
    echo "</tr>";    

  echo "</table>";
echo "</div>";
echo "<br />" . "<br />";

# ---------------------------------------------------------------------------------------------------------------------

echo "<h1>Array més gran i més petit average</h1>";

$array_gran = 0;
$array_petita = 0;
echo $avg;
echo "<br />";
echo "<div style='margin: auto;width: 50%;'>";
  for ($i = 0; $i < $len2; $i++) {
    if ($numrandom2[$i] > $avg) {
      if ($array_gran == 0) {
        $array_gran = $numrandom2[$i] . " ";
      }
      elseif ($i == $len2 - 1) {
        $array_gran = $array_gran . " ";
      }
      else {
        $array_gran = $array_gran . $numrandom2[$i] . " ";
      }
    }
    else {
      if ($array_petita == 0) {
        $array_petita = $numrandom2[$i] . " ";
      }
      elseif ($i == $len2 - 1) {
        $array_petita = $array_petita . " ";
      }
      else {
        $array_petita = $array_petita . $numrandom2[$i] . " ";
      }
    }
  }
echo $array_gran;
echo "<br />";
echo $array_petita;

#      "<table style='border-collapse: collapse;'>";
#          echo"<p style='font-size: 20px; padding-left:1px;'>Multiples of 10</p>";
#          echo "<tr>";
#
#            echo "<th style='font-weight: bold; background-color: rgb(222, 242, 194); padding-left: 10px;'>$num_array_mult_10</th>";
#          
#          echo "<td style='background-color: rgb(222, 242, 194); padding-left: 10px;'>$numrandom2[$i]</td>";
#        echo "</tr>";
#      }
#      echo "</table>";

echo "</div>";
?>