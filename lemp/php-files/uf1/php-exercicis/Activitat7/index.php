<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Activitat 5</title>
        <link rel="stylesheet" type="text/css" href="power.css">
        <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
        <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    </head>
    <body>
        <?php
            include "calcul.php";
        ?>
        <div class="color_fons">
            <h1>Exponent Calculator</h1>
            <div class="container">
                <!--$_SERVER["PHP_SELF"] l'utilitzo per a especificar que l'informació que ha d'agafar el formulari està en aquest mateix fitxer i no en un extern.-->
                <form method="post" action="calcul.php"> 
                    <p>Exponent calculator xn</p>
                </form>
                <div class="table_grey">
                    <div class="x">  
                        <label for="number">x = </label>
                        <input class="numberx" type="number" name="base" id="base" placeholder="Enter a number" value="<?php echo $base;?>" require/><br />
                    </div>
                    <div class="n">
                        <label for="number">n = </label>
                        <input class="numbern" type="number" name="expo" id="expo" placeholder="Enter a number" value="<?php echo $expo;?>" require/><br /><br />
                    </div>                    
                    <input class="bottom" type="submit" value="CALCULATE" /><br />
                    <div class="resultat">Exponent calculator: </div><br />
                    <div class="resultat"> 
                        nx = <span id="resultat"><?php echo execute() ?></span>
                    </div>
                </div>
            </div>
            <br />
            <h2>Positive exponent</h2>
                Exponentiation is a mathematical operation, written as xn, involving the base x and an
                exponent n. In the case where n is a positive integer, exponentiation corresponds to
                repeated multiplication of the base, n times.<br />                
                <center><img src="images/positive.png"></center><br />
                The calculator above accepts negative bases, but does not compute imaginary numbers. It
                also does not accept fractions, but can be used to compute fractional exponents, as long
                as the exponents are input in their decimal form.
            <br /><br />
            <h2>Negative exponent</h2>
                When an exponent is negative, the negative sign is removed by reciprocating the base and
                raising it to the positive exponent.<br />
                <center><img src="images/negative.png"></center><br />
                So the factorial n! is equal to the number n times the factorial of n minus one. This recurses
                until n is equal to 0.
            <br /><br />
            <h2>Exponent 0</h2>
                When an exponent is 0, any number raised to the 0 power is 1.<br />
                <center><img src="images/exponent0.png"></center><br />
                For 0 raised to the 0 power the answer is 1 however this is considered a definition and not
                an actual calculation.
            <br /><br />
            <h2>Real number exponent</h2>
                For real numbers, we use the PHP function pow(n,x).
    </body>
</html>