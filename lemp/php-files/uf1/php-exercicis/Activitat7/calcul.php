<?php
    function execute() {

        $base = "";
        $expo = "";

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $base = validate($_POST["base"]);
            $expo = validate($_POST["expo"]);
        }

        return calcularPotencial($base, $expo);
    }

    function validate($data){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;

    }

    function calcularPotencial($num, $exponent) {
        return pow($num, $exponent);
    }
?>