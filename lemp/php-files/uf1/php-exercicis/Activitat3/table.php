<?php

  echo "<table>";
      echo "<tr>";
        echo "<th>Degrees</th>";
        echo "<th>Radians</th>";
        echo "<th>Sine</th>";
        echo "<th>Cosine</th>";
      echo "</tr>";
      for ($i = 0; $i <= 360; $i++){
      echo "<tr>";
        $radian=round(deg2rad($i), 4);
        $sin=round(sin($radian),4);
        $cos=round(cos($radian),4);
        echo "<td>" . $i . "</td=>";
        echo "<td>" . $radian .  "</td>";
        if ( $sin >= 0 ) {
          echo "<td class='positive'>" . $sin .  "</td>";
        }
        else {
          echo "<td class='negative'>" . $sin .  "</td>";
        }
        
        if ( $cos >= 0 ) {
          echo "<td class='positive'>" . $cos .  "</td>";
        }
        else {
          echo "<td class='negative'>" . $cos .  "</td>";
        }
      echo "</tr>";
    }


  echo "</table>";
?>