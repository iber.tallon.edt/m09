<?php
class Multiples5and7
{

  /*1. Multiples of 5 or 7
Suma tots els els nombres naturals múltiples de 5 o 7 que hi ha entre 0 i un número passat per argument.
Per exemple per, si l’argument és 10, el resultat serà 5 + 7 + 10 = 22.
Si l’argument és negatiu o zero, retorna 0.

Nota: si el nombre és múltiple de 5 i 7 a la vegada, només el sumarà un cop.

Exemples:

multiples5and7(10); 	// return 22
multiples5and7(100); 	// return 1680

Executar proves:
Obrir terminal.
Siturar-se al directori "tests".
Executar el test unitari: 
php phpunit.phar .\unit\Multiples5and7Test.php


*/

  public function multiples5and7(int $number): int
  {
    //TODO
    return 0;
  }
}
