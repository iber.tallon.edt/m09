<?php
//require_once("assets/php/country.php");
//require_once("assets/php/address.php");

$servername = "172.22.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";
$port = 8081;


// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM address a INNER JOIN country c ON a.country_id = c.country_id ORDER BY a.address_id;";
$stmt = $conn->prepare($sql);
$stmt->execute();

$result = $stmt->get_result(); 

if ($result->num_rows > 0) {
    
    echo "Number of records: " . $result->num_rows;
    echo "<table>";
        echo "<tr>";
            echo "<td>address_id</td>";
            echo "<td>street_number</td>";
            echo "<td>street_name</td>";
            echo "<td>city</td>";
            echo "<td>country_id</td>";
            echo "<td>country_name</td>";
        echo "</tr>";
    while ($row = $result->fetch_assoc()) {
        echo "<tr>";
            echo "<td>". $row["address_id"] ."</td>";
            echo "<td>". $row["street_number"] ."</td>";
            echo "<td>". $row["street_name"] ."</td>";
            echo "<td>". $row["city"] ."</td>";
            echo "<td>". $row["country_id"] ."</td>";
            echo "<td>". $row["country_name"] ."</td>";
        echo "</tr>";
    }
} else {
    echo "0 results";
}     echo "</table>";

$stmt->close();
$conn->close();

?>