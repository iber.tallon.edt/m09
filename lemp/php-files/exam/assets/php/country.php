<?php
class Country
{
    // Properties
    private int $country_id;
    private string $country_name;

}

// Constructor
function __construct(int $country_id, string $country_name) {
    $this->country_id = $country_id;
    $this->country_name = $country_name;
}

//Getters
function get_country_id(): int {
    return $this->country_id;
}
function get_country_name(): string {
    return $this->country_name;
}

//Setters

function set_country_id(int $country_id): void {
    $this->country_id = $country_id;
}
function set_country_name(string $country_name): void {
    $this->country_name = $country_name;
}

//toString
public function __toString(): string {
    return "Country:\n" . 
    "country_id: " . $this->get_country_id() . "\n" .
    "country_name: " . $this->get_country_name() . "\n";
}
?>





