<?php
require_once('country.php');

class Address
{
    // Properties
    private int $address_id;
    private string $street_number;
    private string $street_name;
    private string $city;
    private Country $country;

}

// Constructor
function __construct($address_id, $street_number, $street_name, $city)
{
    $this->$address_id = $address_id;
    $this->street_number = $street_number;
    $this->street_name = $street_name;
    $this->city = $city;
}

//Getters
function get_address_id(): int {
    return $this->address_id;
}

function get_street_number(): string {
    return $this->street_number;
}

function get_street_name(): string {
    return $this->street_name;
}

function get_city(): string {
    return $this->city;
}

function get_country(): Country {
    return $this->country;
}

//Setters

function set_address_id(int $address_id): void {
    $this->address_id = $address_id;
}
function set_street_number(string $street_number): void {
    $this->street_number = $street_number;
}
function set_street_name(string $street_name): void {
    $this->street_name = $street_name;
}
function set_city(string $city): void {
    $this->city = $city;
}

function set_country(Country $country): Country {
    $this->country = $country;
}

//toString
public function __toString(): string {
    return "Address:\n" . 
    "address_id: " . $this->get_address_id() . "\n" .
    "street_number: " . $this->get_street_number() . "\n";
    "street_name: " . $this->get_street_name() . "\n";
    "city: " . $this->get_city() . "\n";
    "country: " . $this->get_country() . "\n";
}
?>