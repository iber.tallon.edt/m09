<?php

class Status
{
    // Properties
    private int $status_id;
    private string $status_name;
    
    // Constructor
    function __construct(int $status_id, string $status_name){
        $this->status_id = $status_id;
        $this->status_name = $status_name;
    }

    // Getters and setters
    function get_status_id(): int {
        return $this->status_id;
    }

    function set_status_id(int $status_id): void {
        $this->status_id = $status_id;
    }

    function get_status_name(): string {
        return $this->status_name;
    }

    function set_user_id(string $status_name): void {
        $this->status_name = $status_name;
    }

    // toString
    public function __toString(): string {
        return "User: " . 
        "Status id: " . $this->get_status_id() . "\n" . 
        "Status name: " . $this->get_status_name() . "\n";
    }

}

