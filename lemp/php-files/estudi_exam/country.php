<?php

class Country 

// Propieties
{
    private int $country_id;
    private int $country_name;


// Constructor
function __construct(int $country_id, int $country_name) {
    $this->country_id = $country_id;
    $this->country_name = $country_name;
}

// Getters
function get_country_id(): int {
    return $this->country_id;
}
function get_country_name(): int {
    return $this->country_name;
}

// Setters
function set_country_id(int $country_id): void {
    $this->country_id = $country_id;
}
function set_country_name(int $country_name): void {
    $this->country_name = $country_name;
}

// toString
public function __toString(): string {
    return "Country: " . "\n" .
    "Country id: " . $this->get_country_id() . "\n" .
    "Country name: " . $this->get_country_name();
}

}
?>