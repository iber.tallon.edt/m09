<?php

include_once("status.php");
class User
{
    // Properties
    private int $user_id;
    private string $user_first_name;
    private string $user_last_name;
    private string $user_email;
    private string $user_password;
    private Status $status;
    private string $user_info;


    // Constructor
    function __construct(int $user_id, string $user_first_name, string $user_last_name, string $user_email, string $user_password, Status $status, string $user_info)
    {
        $this->user_id = $user_id;
        $this->user_first_name = $user_first_name;
        $this->user_last_name = $user_last_name;
        $this->user_email = $user_email;
        $this->user_password = $user_password;
        $this->status = $status;
        $this->user_info = $user_info;
    }

    // Getters
    function get_user_id(): int {
        return $this->user_id;
    }

    function get_first_name(): string {
        return $this->user_first_name;
    }

    function get_last_name(): string {
        return $this->user_last_name;
    }

    function get_user_email(): string {
        return $this->user_email;
    }

    function get_user_password(): string {
        return $this->user_password;
    }

    function get_status(): Status {
        return $this->status;
    }

    function get_user_info(): string {
        return $this->user_info;
    }


    // Setters
    function set_user_id(int $user_id): void {
        $this->user_id = $user_id;
    }

    function set_user_first_name(string $user_first_name): void {
        $this->user_first_name = $user_first_name;
    }

    function set_user_last_name(string $user_last_name): void {
        $this->user_last_name = $user_last_name;
    }

    function set_user_email(string $user_email): void {
        $this->user_email = $user_email;
    }

    function set_user_password(string $user_password): void {
        $this->user_password = $user_password;
    }

    function set_status(Status $status): void {
        $this->status = $status;
    }

    function set_user_info(string $user_info): void {
        $this->user_info = $user_info;
    }


    // toString

    public function __toString(): string
    {
        return "User: " .
        "User id: ". $this->user_id . "\n" .
        "First name: " . $this->user_first_name . "\n" .
        "Last name: " . $this->user_last_name . "\n" . 
        "User email: " . $this->user_email . "\n" . 
        "User password: " . $this->user_password . "\n" . 
        "Status: " . $this->status . "\n" . 
        "User info: " . $this->user_info . "\n";
    }

}
?>