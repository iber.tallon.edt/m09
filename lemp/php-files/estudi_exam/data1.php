<?php

include_once("address.php");
include_once("country.php");

$servername = "172.19.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

// Creació de la connexió amb la base de dades
$conn = new mysqli($servername, $username, $password, $dbname);

// Confirmar l'estat de la connexió
if ($conn -> connect_error) {
    die("Connection failed: " . $conn -> connect_error);
} else {
    echo("Successfully connected" . "<br/>");
}

$sql = "SELECT * FROM address a
INNER JOIN country c ON a.country_id = c.country_id
ORDER BY a.address_id;";

$result = $conn->query($sql);

echo "<h2>Addresses</h2>";

echo "Number of records: " . $result->num_rows ."<br>";

echo "<table>";

    echo "<tr>";
        echo "<td>address_id</td>";
        echo "<td>street_number</td>";
        echo "<td>street_name</td>";
        echo "<td>city</td>";
        echo "<td>country_id</td>";
        echo "<td>country_name</td>";
    echo "</tr>";

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $address = new Address($row["address_id"], $row["street_number"], $row["street_name"], $row["city"], $country);
        $country = new Country($row["country_id"], $row["country_name"]);
        echo "<tr>";
            echo "td" . $address->get_address_id() . "</td>";
            echo "td" . $address->get_street_number() . "</td>";
            echo "td" . $address->get_street_name() . "</td>";
            echo "td" . $address->get_city() . "</td>";
            echo "td" . $country->get_country_id() . "</td>";
            echo "td" . $country->get_country_name() . "</td>";
        echo "</tr>";
    }
}

echo "</table>";

$conn->close();

?>