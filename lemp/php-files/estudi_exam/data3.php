<?php
$servername = "172.21.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

require_once("user.php");
require_once("status.php");

$sql = "INSERT INTO users (user_first_name, user_last_name, user_email, user_password, user_status_id, user_info) 
        VALUES (?, ?, ?, ?, ?, ?)";
$stmt = $conn->prepare($sql);
$stmt->bind_param("ssssis", $user_firt_name, $user_last_name, $user_email, $user_password, $user_status, $user_info);

$user_firt_name = "Pere";
$user_last_name = "Gonzalez"; 
$user_email = "pere@gmail.com"; 
$user_password = "1234"; 
$user_status = 1; 
$user_info = "User de pere"; 

$stmt->execute();

if($stmt->affected_rows > 0) {
    echo "Num rows inserted: " . $stmt->affected_rows;
} else {
    echo "Error when inserting data";
}

$stmt->close();
$conn->close();
?>

