<?php   

// Importar classes
include_once("address.php");
include_once("country.php");

$servername = "172.21.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

// Crear la connexió
$conn = new mysqli($servername, $username, $password, $dbname);

// Mirar estat de la connexió
if ($conn->connect_error) {
    die("Connection failed: " . $conn -> connect_error);
} else {
    echo("Successfully connected" . "<br/>");
}

// Crear consulta

echo "<>";
    echo "<input type='text' name='city'>";
    echo "<button class='boton' type='submit' name='execute'>Search</button>";
echo "</form>";

// funció que busca la ciutat

function execute($conn, $city) {
    $sql = "SELECT * FROM address a
    INNER JOIN country c ON a.country_id = c.country_id
    WHERE LOWER(a.city) LIKE ?
    ORDER BY a.address_id;";

    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s", $city);
    $stmt->execute();
    $result = $stmt->get_result();

    echo "Number of records: " . $result->num_rows . "<br>";

    echo "<table class='table'>";
        echo "<tr>";
            echo "<td>address_id</td>";
            echo "<td>street_number</td>";
            echo "<td>street_name</td>";
            echo "<td>city</td>";
            echo "<td>country_id</td>";
            echo "<td>country_name</td>";
        echo "</tr>";

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $country = new Country($row["country_id"], $row["country_name"]);
                $address = new Address($row["address_id"], $row["street_number"], $row["street_name"], $row["city"], $row["country_id"]);

                echo "<tr>";
                    echo "<td>" . $address->get_address_id() . "</td>";
                    echo "<td>" . $address->get_street_number() . "</td>";
                    echo "<td>" . $address->get_street_name() . "</td>";
                    echo "<td>" . $address->get_city() . "</td>";
                    echo "<td>" . $country->get_country_id() . "</td>";
                    echo "<td>" . $country->get_country_name() . "</td>";
                echo "</tr>";
            }
        }
    echo "</table>";  
}

if (isset($_GET['execute'])) {
    $city = "%" . $_GET['city'] . "%";
    execute($conn, $city);
}

$stmt->close();
$conn->close();

?>