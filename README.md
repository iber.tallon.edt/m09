# 🏫  EDT AISX-M09 2023/2024

- [Aula català](#📂-aula-català)
- [Aula castellano](#📂-aula-castellano)

## 📂 Aula català
###  Arrelació dels repositoris del projecte M09
```bash
|- lemp
|  |- nginx-conf     
|  |- php-files
|  |- |- php-exemples
|  |- |- |- ex1
|  |- |- |- ex2
|  |- |- |- ex3
|  |- |- |- ex4
|  |- |- |- ex5
|  |- |- |- ex6
|  |- |- |- ex7
|  |- |- |- ex8
|  |- |- php-exercicis 
|  |- |- |- Activitat1
|  |- |- |- Activitat2
|  |- |- |- Activitat3
|  |- compose.yml
|  |- php
|  |- php-dockerfile
```

Aquest projecte consisteix en aprendre conceptes bàsics de programació amb el llenguatge PHP per poder desenvolupar pàgines webs dinàmiques.   

🖐 Dels repositoris ensenyats, destaquem dos: <b>lemp</b> i <b>php-files</b>.

El repositori 📂 `lemp` conté tota la configuració amb la qual podrem arrancar els varis serveis que tinguem al fitxer 👉 <b>compose.yml</b>.

El repositori 📂 `php-files` conté tots els fitxers que utilitzo tant per fer <i>proves</i> com per fer <i>activitats</i> amb el <b>servei PHP</b>.

<br />

## 📂 Aula Castellano


###  Estructuración de los repositorios del proyecto M09
```bash
|- lemp
|  |- nginx-conf     
|  |- php-files
|  |- |- php-exemples
|  |- |- |- ex1
|  |- |- |- ex2
|  |- |- |- ex3
|  |- |- |- ex4
|  |- |- |- ex5
|  |- |- |- ex6
|  |- |- |- ex7
|  |- |- |- ex8
|  |- |- php-exercicis 
|  |- |- |- Activitat1
|  |- |- |- Activitat2
|  |- |- |- Activitat3
|  |- compose.yml
|  |- php
|  |- php-dockerfile
```

Este proyecto consiste en aprender conceptos básicos de programación con el lenguaje PHP para poder desarrollar páginas web dinámicas. 

🖐 De los repositorios enseñados, destacamos dos: <b>lemp</b> i <b>php-files</b>.

El repositorio 📂 `lemp` contiene toda la configuración con la que podremos arrancar los diversos servicios que tengamos en el archivo 👉 <b>compose.yml</b>.

El repositorio 📂 `php-files` contiene todos los archivos que utilizo tanto para realizar <i>pruebas</i> como para realizar <i>actividades</i> con el <b>servicio PHP</b>.